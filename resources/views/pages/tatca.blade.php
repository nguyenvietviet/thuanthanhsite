@extends('layout.index')
@section('title')
Loại tin
@endsection

@section('content')
   <section class="hero-wrap hero-wrap-2" style="background-image: url('theme_page/images/bg_1.jpg');">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-2 bread">Tin</h1>
            <p class="breadcrumbs"><span class="mr-2"><a href="{{route('home')}}">Trang chủ <i class="ion-ios-arrow-forward"></i></a></span> <span>{{$categories->name}} <i class="ion-ios-arrow-forward"></i></span></p>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section bg-light">
         <div class="container">
            <div class="row">

@foreach($parent as $parents)
   <?php
       $news = App\NewCate::where('category_id',$parents->id)->paginate(3);
    ?>
    @foreach($news as $n)

          <div class="col-md-6 col-lg-4 ftco-animate">
            <div class="blog-entry">
              <a href="tin-tuc/{{$n->id}}/{{$n->slug}}.html" class="block-20 d-flex align-items-end" style="background-image: url('upload/{{$n->image}}');">
                        <div class="meta-date text-center p-2">
                <span>{{$n->created_at}}</span>
                </div>
              </a>
              <div class="text bg-white p-4">
                <h3 class="heading" style="overflow: hidden;height: 25px;"><a href="tin-tuc/{{$n->id}}/{{$n->slug}}.html">{{$n->title}}</a></h3>
                <p>{!!$n->tomtat!!}</p>
                <div class="d-flex align-items-center mt-4">
                   <p class="mb-0"><a href="tin-tuc/{{$n->id}}/{{$n->slug}}.html" class="btn btn-primary">Xem chi tiết <span class="ion-ios-arrow-round-forward"></span></a></p>
                   <!--<p class="ml-auto mb-0">
                     <a href="#" class="mr-2">Admin</a>
                   </p>-->
                </div>
              </div>
            </div>
          </div>
@endforeach
@endforeach
          
        </div>
        {{-- <div class="row mt-5">
          <div class="col text-center">
            <div class="block-27">
              {{$news->links()}}
            </div>
          </div>
        </div> --}}
         </div>
      </section>

      @include('pages.rating')
@endsection