<section class="ftco-section testimony-section">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-8 text-center heading-section ftco-animate">
            <h2 class="mb-4">Nhận xét của khách hàng</h2>
            <p>Những đánh giá chân thật nhất của khách hàng sẽ giúp cho chúng tôi có thể hoàn hiện công ty hơn và ngày càng có thể phát triển để đáp ứng được mọi nhu cầu của khách hàng mỗi khi tìm đến chúng tôi</p>
          </div>
        </div>
        <div class="row ftco-animate justify-content-center">
          <div class="col-md-12">
            <div class="carousel-testimony owl-carousel">
<?php 
  $rating = App\Rating::all();
?>

            @foreach($rating as $ra)
              <div class="item" style="height: 280px;">
                <div class="testimony-wrap d-flex">
                  <div class="user-img" style="background-image: url(rating/{{$ra->r_avatar}})">
                  </div>
                  <div class="text pl-4">
                    <span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                    <p style="overflow: hidden;height: 140px;">{!!$ra->r_content!!}</p>
                    <p class="name">{{$ra->r_author}}</p>
                    <span class="position">{{$ra->r_title}}</span>
                  </div>
                </div>
              </div>
            @endforeach
            </div>
          </div>
        </div>
      </div>
    </section>