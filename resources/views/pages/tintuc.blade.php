@extends('layout.index')
@section('title')
tin tức
@endsection

@section('content')
   
     <section class="hero-wrap hero-wrap-2" style="background-image: url('theme_page/images/bg_1.jpg');">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            {{-- <h1 class="mb-2 bread">Chi tiết</h1> --}}
            <h2 class="mb-2 bread"><span class="mr-2"><a>Trang chủ <i class="ion-ios-arrow-forward"></i></a></span> 
 {{-- @if(isset($categories)) --}}
        <span class="mr-2"><a><span>{{$news->category->name}} </a></span>
{{-- @endif --}}
               
            </h2>
          </div>
        </div>
      </div>
    </section>


 <section class="ftco-section" style="padding:4em 0 4em 0;background: #ebf0f1;">
         <div class="container" style="background: white; padding: 2%;">

    <section class="ftco-section" style="padding:4em 0 0;">
         <div class="container"{{--  style="background: white; padding: 2%;" --}}>
            <div class="row">
          <div class="col-lg-12 ftco-animate">
            <h2 class="mb-3 text-center">{{$news->title}}</h2>
            <p> <strong>{!!$news->tomtat!!} </strong></p>
            <p>{!!$news->content!!}</p>
            <div class="tag-widget post-tag-container mb-5 mt-5">
              <div class="tagcloud">
               @foreach($newHot as $n)
                <a href="tin-tuc/{{$n->id}}/{{$n->slug}}.html" class="tag-cloud-link">{{$n->title}}</a>
               @endforeach
              </div>
            </div>
          </div> <!-- .col-md-8 -->
        </div>
         </div>
      </section>
  </div>
  <hr>

      {{-- <section class="ftco-section testimony-section" style="margin-top: 0px; background-color: #fafafa !important">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-8 text-center heading-section ftco-animate">
            <h2 class="mb-4">NHẬN XÉT CỦA KHÁCH HÀNG</h2>
            <p>Những đánh giá trân thật nhất của khách hàng sẽ giúp cho chúng tôi có thể hoàn hiện công ty hơn và ngày càng có thể phát triển để đáp ứng được mọi nhu cầu của khách hàng mỗi khi tìm đến chúng tôi</p>
          </div>
        </div>
        <div class="row ftco-animate justify-content-center">
          <div class="col-md-12">
            <div class="carousel-testimony owl-carousel">
            @foreach($rating as $ra)
              <div class="item" style="height: 280px;">
                <div class="testimony-wrap d-flex">
                  <div class="user-img" style="background-image: url(rating/{{$ra->r_avatar}})">
                  </div>
                  <div class="text pl-4">
                    <span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                    <p style="overflow: hidden;height: 140px;">{!!$ra->r_content!!}</p>
                    <p class="name">{{$ra->r_author}}</p>
                    <span class="position">{{$ra->r_title}}</span>
                  </div>
                </div>
              </div>
            @endforeach
            </div>
          </div>
        </div>
      </div>
    </section> --}}

@endsection












