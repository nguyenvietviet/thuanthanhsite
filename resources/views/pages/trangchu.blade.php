@extends('layout.index')
@section('title')
Trang chủ
@endsection
@section('content')
<!--Page-Content-->  
<!--Slide-->
@include('layout.slide')

<section class="ftco-section">
      <div class="container">
        <div class="row d-flex">
          <div class="col-md-12 wrap-about pr-md-4 ftco-animate">
            <h2 class="mb-4 text-center">Dịch Vụ Của Chúng Tôi</h2>
            <p class="text-center">Thuanthanhtech - Dịch vụ chuyên nghiệp - Công nghệ vượt trội</p>
            <div class="row mt-5">

            @foreach($newSlide as $ar)
                    <div class="col-md-4" >
                      <div class="services text-center">
                        <div class="icon mt-2 d-flex justify-content-center align-items-center">
                        <img style="height: 60px;" src="upload/{{$ar->icon}}"></span>
                        </div>
                        <div class="text media-body" style="text-overflow: ellipsis;overflow: hidden; height:200px;white-space: normal;">
                          <h3><a href="tin-tuc/{{$ar->id}}/{{$ar->slug}}.html">{{$ar->title}}</a></h3>
                          <p style="margin-top:-10px;">{!!$ar->tomtat!!}</p>
                        </div>
                      </div>
              </div>
            @endforeach
             
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="ftco-intro ftco-no-pb img" style="background-image: url(theme_page/images/bg_3.jpg);">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-10 text-center heading-section heading-section-white ftco-animate">
            <h2 class="mb-0">BẠN LUÔN CÓ ĐƯỢC SỰ PHỤC VỤ TỐT NHẤT CỦA CHÚNG TÔI</h2>
          </div>
        </div>  
      </div>
    </section>

    <section class="ftco-counter" id="section-counter">
      <div class="container">
        <div class="row d-md-flex align-items-center justify-content-center">
          <div class="wrapper">
            <div class="row d-md-flex align-items-center">
              <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18">
                  <div class="icon"><span class="flaticon-doctor"></span></div>
                  <div class="text">
                    <strong class="number" data-number="204">0</strong>
                    <span>DỰ ÁN ĐÃ HOÀN THÀNH</span>
                  </div>
                </div>
              </div>
              <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18">
                  <div class="icon"><span class="flaticon-doctor"></span></div>
                  <div class="text">
                    <strong class="number" data-number="420">0</strong>
                    <span>KHÁCH HÀNG HÀI LÒNG</span>
                  </div>
                </div>
              </div>
              <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18">
                  <div class="icon"><span class="flaticon-doctor"></span></div>
                  <div class="text">
                    <strong class="number" data-number="15">0</strong>
                    <span>GIẢI THƯỞNG ĐÃ NHẬN</span>
                  </div>
                </div>
              </div>
              <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18">
                  <div class="icon"><span class="flaticon-doctor"></span></div>
                  <div class="text">
                    <strong class="number" data-number="4">0</strong>
                    <span>NĂM KINH NGHIỆM</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

{{--!Tại sao lại chọn chúng tôi--}}
    <section class="ftco-section">
      <div class="container">
        <div class="row justify-content-center mb-5 pb-2">
          <div class="col-md-8 text-center heading-section ftco-animate">
            <h2 class="mb-4">TẠI SAO LẠI CHỌN CÔNG TY CHÚNG TÔI ?</h2>
            <p>Với tầm nhìn dài hạn và quan điểm phát triển bền vững, Thuanthanhtech đang tập trung đầu tư vào lĩnh vực phần mềm trực tuyến (website, văn phòng điện tử, sổ liên lạc điện tử, …),dịch vụ hỗ trợ doanh nghiệp quảng bá thương hiệu (marketing online), dịch vụ giải trí (phim, clip, …) trên di động.</p>
          </div>
        </div>
        <div class="row no-gutters">
          @foreach($about as $ab)
          <div class="col-lg-4 d-flex" style="border: 1px solid #eee;">
            <div class="services-2 noborder-left text-center ftco-animate">
              <div class="icon mt-2 d-flex justify-content-center align-items-center"><img style="height: 55px;" src="about/{{$ab->about_avatar}}"></div>
              <div class="text media-body">
                <h3 style="margin-top: 10px;">{{$ab->about_title}}</h3>
                <p>{!!$ab->about_content!!}</p>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </section>
{{--!End Tại sao lại chọn chúng tôi--}}  
{{--!Khách hàng tiêu biểu--}}

<section class="ftco-section ftco-no-pb">
   <div class="container-fluid px-0">
      <div class="row no-gutters justify-content-center mb-5">
         <div class="col-md-7 text-center heading-section ftco-animate">
            <h2 class="mb-4">KHÁCH HÀNG TIÊU BIỂU</h2>
         </div>
      </div>
      <div class="row no-gutters">
         <div class="col-md-3">
            <div class="project img ftco-animate d-flex justify-content-center align-items-center" style="background-image: url(theme_page/images/dt1.png);">
               <a href="#" class="btn-site d-flex align-items-center justify-content-center"><span class="icon-subdirectory_arrow_right"></span></a>
            </div>
         </div>
         <div class="col-md-3">
            <div class="project img ftco-animate d-flex justify-content-center align-items-center" style="background-image: url(theme_page/images/dt2.png);">
               <a href="#" class="btn-site d-flex align-items-center justify-content-center"><span class="icon-subdirectory_arrow_right"></span></a>
            </div>
         </div>
         <div class="col-md-3">
            <div class="project img ftco-animate d-flex justify-content-center align-items-center" style="background-image: url(theme_page/images/dt3.png);">
            </div>
         </div>
         <div class="col-md-3">
            <div class="project img ftco-animate d-flex justify-content-center align-items-center" style="background-image: url(theme_page/images/dt4.png);">
               <a href="#" class="btn-site d-flex align-items-center justify-content-center"><span class="icon-subdirectory_arrow_right"></span></a>
            </div>
         </div>
      </div>
   </div>
</section>

{{--!End khách hàng tiêu biểu--}}  






    <section class="ftco-section bg-light">
      <div class="container">
        <div class="row justify-content-center mb-5 pb-2">
          <div class="col-md-8 text-center heading-section ftco-animate">
            <h2 class="mb-4">TIN NỔI BẬT</h2>
            <p>Separated they live in. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p>
          </div>
        </div>
        <div class="row">
          @foreach($newHot as $ar)
          <div class="col-md-6 col-lg-4 ftco-animate">
            <div class="blog-entry">
              <a href="tin-tuc/{{$ar->id}}/{{$ar->slug}}.html" class="block-20 d-flex align-items-end" style="background-image: url('upload/{{$ar->image}}');">
                <div class="meta-date text-center p-2">
                 <span>{{$ar->created_at}}</span>
                </div>
              </a>
              <div class="text bg-white p-4">
                <h3 class="heading" style="overflow: hidden;height: 25px;"><a href="#">{{$ar->title}}</a></h3>
                <p>{!!$ar->tomtat!!}</p>
                <div class="d-flex align-items-center mt-4">
                  <p class="mb-0"><a href="tin-tuc/{{$ar->id}}/{{$ar->slug}}.html" class="btn btn-primary">Chi tiết <span class="ion-ios-arrow-round-forward"></span></a></p>
                </div>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </section>

    @include('pages.rating')

@endsection