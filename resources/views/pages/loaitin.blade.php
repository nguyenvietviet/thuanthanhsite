@extends('layout.index')
@section('title')
Loại tin
@endsection

@section('content')
   <section class="hero-wrap hero-wrap-2" style="background-image: url('theme_page/images/bg_1.jpg');">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-2 bread">Tin</h1>
            <p class="breadcrumbs"><span class="mr-2"><a href="{{route('home')}}">Trang chủ <i class="ion-ios-arrow-forward"></i></a></span> <span>{{$categories->name}} <i class="ion-ios-arrow-forward"></i></span></p>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section bg-light">
         <div class="container">
            <div class="row">

{{-- @foreach($news as $ar)
            <div class="col-md-6 col-lg-4">
                <div class="card" style="width: 18rem;">
                  <img class="card-img-top" style="height: 200px;width: 100%;" src="upload/{{$ar->image}}" alt="Card image cap">
                  <div class="card-body">
                    <h5 class="card-title">{{$ar->title}}</h5>
                    <p class="card-text">{!!$ar->tomtat!!}</p>
                    <div class="d-flex align-items-center mt-4">
                   <p class="mb-0"><a href="tin-tuc/{{$ar->id}}/{{$ar->slug}}.html" class="btn btn-primary">Xem chi tiết <span class="ion-ios-arrow-round-forward"></span></a></p>
                </div>
                  </div>
                </div>
            </div>
@endforeach --}}
      @foreach($news as $ar)
          <div class="col-md-6 col-lg-4 ftco-animate">
            <div class="blog-entry">
              <a href="tin-tuc/{{$ar->id}}/{{$ar->slug}}.html" class="block-20 d-flex align-items-end"><img width="450px;" src="upload/{{$ar->image}}">

              {{--   <a href="tin-tuc/{{$ar->id}}/{{$ar->slug}}.html" class="block-20 d-flex align-items-end" style="background-image: url('upload/{{$ar->image}}');"><img src="upload/{{$ar->image}}">
                <div class="meta-date text-center p-2">
                     <span>{{$ar->created_at}}</span>
                </div> --}}


              </a>
              <div class="text bg-white p-4">
                <h3 class="heading" style="overflow: hidden;height: 25px;"><a href="tin-tuc/{{$ar->id}}/{{$ar->slug}}.html">{{$ar->title}}</a></h3>
                <p>{!!$ar->tomtat!!}</p>
                <div class="d-flex align-items-center mt-4">
                   <p class="mb-0"><a href="tin-tuc/{{$ar->id}}/{{$ar->slug}}.html" class="btn btn-primary">Xem chi tiết <span class="ion-ios-arrow-round-forward"></span></a></p>
                </div>
              </div>
            </div>
          </div>
          @endforeach
          
        </div>
        <div class="row mt-5">
          <div class="col text-center">
            <div class="block-27">
              {{$news->links()}}
            </div>
          </div>
        </div>
         </div>
      </section>

      @include('pages.rating')
@endsection