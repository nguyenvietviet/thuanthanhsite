@extends('layout.index')
@section('title')
	Liên hệ
@endsection

@section('content')

<!--End Model Liên Hệ -->


 <section class="hero-wrap hero-wrap-2" style="background-image: url('theme_page/images/bg_1.jpg');">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-2 bread">Liên Hệ Chúng Tôi</h1>
            <p class="breadcrumbs"><span class="mr-2"><a href="{{route('home')}}">Trang chủ <i class="ion-ios-arrow-forward"></i></a></span> <span>Liên hệ <i class="ion-ios-arrow-forward"></i></span></p>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section contact-section">
      <div class="container">
        <div class="row d-flex  contact-info justify-content-center">
         <div class="col-md-8">
            <div class="row mb-5">
                <div class="col-md-4 text-center">
                  <div class="icon">
                     <span class="icon-map-o"></span>
                  </div>
                  <p><span>Địa chỉ:</span> Số 9, Nguyễn Xiển, Thanh Xuân, Hà Nội.</p>
                </div>
                <div class="col-md-4 text-center border-height">
                  <div class="icon">
                     <span class="icon-mobile-phone"></span>
                  </div>
                  <p><span>Điện thoại:</span> <a href="tel://1234567920">+ 1235 2355 98</a></p>
                </div>
                <div class="col-md-4 text-center">
                  <div class="icon">
                     <span class="icon-envelope-o"></span>
                  </div>
                  <p><span>Email:</span>contact@thuanthanhtech.com</p>
                </div>
              </div>
          </div>
        </div>
      </div>

      <div class="row block-9 justify-content-center mb-5">
          <div class="col-md-8 mb-md-5 block-9 justify-content-center mb-5">
           
             <form action="" method="post" class="bg-light p-5 contact-form">
               @csrf

              <div class="form-group">
                <input type="text" name="c_name" class="form-control" placeholder="Họ Tên*">
              </div>

              <div class="form-group">
                <input type="email" name="c_email" class="form-control" placeholder="Email*" required="Bạn chưa nhập địa chỉ email">
              </div>

               <div class="form-group">
                <input type="text" name="c_address" class="form-control" placeholder="Địa Chỉ*">
              </div>
             
              <div class="form-group">
                <input type="text" name="c_phone" class="form-control" placeholder="Số Điện Thoại*">
              </div>

              <div class="form-group">
                <input type="text" name="c_title" class="form-control" placeholder="Tiêu Đề*" required="Bạn chưa nhập tiêu đề">
              </div>

              <div class="form-group">
                <textarea cols="30" name="c_content" rows="3" class="form-control" placeholder="Nội Dung*"></textarea>
              </div>

              <div class="form-group submit">
                <input type="submit" value="Gửi Thông Tin" class="btn btn-primary">
              </div>

            </form>
          
          </div>
        </div>
    </section>

    <section class="ftco-section ftco-no-pb ftco-no-pt" style="margin: 0;">
      <div class="container-fluid px-0">
         <div class="col-md-12">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3725.0570134489376!2d105.80226161482004!3d20.990351594464528!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135acbfb81c4d15%3A0x7358bc6b15c5b06a!2zOSDEkMaw4budbmcgTmd1eeG7hW4gWGnhu4NuLCBUaGFuaCBYdcOibiBOYW0sIFRoYW5oIFh1w6JuLCBIw6AgTuG7mWksIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1578295120137!5m2!1svi!2s" width="100%;" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
         </div>
      </div>
    </section>

@endsection 
