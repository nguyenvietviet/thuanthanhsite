<div class="col-md-3 col-sm-3 col-xs-12">
   <section class="top-story">
      <ul class="tab-header">
         <li class="active"><a href="#most-read" data-toggle="tab">Tin mới</a></li>
         <!-- <li class=""><a href="#most-read7" data-toggle="tab">Trong tuần</a></li>-->
         <div class="clear"></div>
      </ul>
      <div id="top-story" class="tab-content">
         <div class="tab-pane active" id="most-read" style="">

            <?php 
               $newRight = App\NewCate::orderBy('created_at','ASC')->where('newType',1)->limit(6)->get();
            ?>
            @foreach($newRight as $new)
            <article data-aid="112">
               <figure>
                  <a href="">
                  <img src="" alt="">
                  </a>
               </figure>
               <header>
                  <h2>
                     <a href="tin-tuc/{{$new->id}}/{{$new->slug}}.html">{{$new->title}}</a>
                  </h2>
               </header>
               <div class="clear"></div>
            </article>
            @endforeach   
         </div>
      </div>
   </section>
</div>