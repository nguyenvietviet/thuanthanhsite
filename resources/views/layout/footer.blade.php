 <footer class="ftco-footer ftco-bg-dark ftco-section">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-lg-4">
            <div class="ftco-footer-widget mb-5">
              <h2 class="ftco-heading-2">Liện hệ theo địa chỉ:</h2>
              <div class="block-23 mb-3">
                <ul>
                  <li><span class="icon icon-map-marker"></span><span class="text">Số 9, Nguyễn Xiển, Thanh Xuân, Hà Nội.</span></li>
                  <li><span class="icon icon-map-marker"></span><span class="text">Thôn Bùi Xá, Xã Ninh Xá, Huyện Thuận Thành, Tỉnh Bắc Ninh.</span></li>
                  <li><span class="icon icon-map-marker"></span><span class="text"> 54/8A Nguyễn Bỉnh Khiêm, Quận 1, TPHCM.</span></li>
                  <li><a href="#"><span class="icon icon-phone"></span><span class="text">(024) 22 134 777 - (+84) 936.160.019</span></a></li>
                  <li><a href="#"><span class="icon icon-envelope"></span><span class="text">contact@thuanthanhtech.com</span></a></li>
                </ul>
              </div>
            </div>
          </div>
          
          <div class="col-md-6 col-lg-4">
            <div class="ftco-footer-widget  ml-md-4">
              <h2 class="ftco-heading-2">Thông tin về chúng tôi:</h2>
              <ul class="list-unstyled">
                <li><a href=""><span class="ion-ios-arrow-round-forward mr-2"></span>Trang chủ</a></li>
                 
              </ul>
            </div>
          </div>

          <div class="col-md-6 col-lg-4">
            <div class="ftco-footer-widget ml-md-4">
              <h2 class="ftco-heading-2">Fanpage:</h2>
              <ul class="ftco-footer-social list-unstyled float-md-left float-left">
                <li class="ftco-animate"><a href=""><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="https://www.facebook.com/thuanthanhtech/"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-youtube"></span></a></li>
              </ul>
            <div class="fb-page" data-href="https://www.facebook.com/thuanthanhtech/" data-tabs="timeline" data-width="350px" data-height="135px" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/thuanthanhtech/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/thuanthanhtech/">Thuận thành Tech</a></blockquote></div>
          </div>
          </div>
          
        </div>
        <div class="row coppyright">
          <div class="col-md-12 text-center">
            <p>{{-- Copyright &copy;<script>document.write(new Date().getFullYear());</script> --}} Copyright © 2018 by  <a href="https://thuanthanhtech.com/" target="_blank">Thuanthanhtech Co., Ltd. </a>All rights reserved</p>
          </div>
        </div>
      </div>
    </footer>
    
