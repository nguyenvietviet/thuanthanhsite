<div class="top-nav-fixed" style="position: fixed;width: 100%;top: 0;left: 0;right: 0;z-index: 1000;">
   <div class="top-contact">
      <div class="max-top-head">
         <div class="uk-clearfix">
            <ul class="infor-top">
               <li>
                  <a href="tel:(949) 480-1639"><i class="icon icon-phone"></i>
                  <span>(+84<span></span>) 936<span></span> 160 019</span></a>
               </li>
               <li>
                  <a class="trigger-scroll" href="mailto:contact@thuanthanhtech.com"><i class="icon icon-envelope"
                     aria-hidden="true"></i>
                  contact@thuanthanhtech.com</a>
               </li>
            </ul>
         </div>
      </div>
   </div>
   <div class="top-menu">
      <div class="max-top-head">
         <div class="row">
            <div class="col-md-2 d-flex align-items-center align-middle logothuanthanh">
               <a class="navbar-brand" href="{{route('home')}}"><img src="theme_page/images/logo.png" style="height: 50px;"></a>
            </div>
            <nav class="col-md-10 navbar navbar-expand-lg navbar-light  ftco-navbar-light" id="ftco-navbar">
               <div class="container d-flex align-items-center">
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav"
                     aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="oi oi-menu"></span> Menu
                  </button>
                  <div class="collapse navbar-collapse" id="ftco-nav">


                     <ul class="navbar-nav mr-auto">
                        <?php
                           $menu_level_1 = App\Category::orderBy('thutu','ASC')->where('parent_id',0)->where('c_publish',1)->get();
                           ?>
                        @foreach($menu_level_1 as $menu1)
                        <li class="nav-item dropdown">
                           <?php 
                              $demb = App\NewCate::where('category_id',$menu1->id)->get();
                              $demb1 = count($demb);

                          ?>

                              <?php 
                              $demc = App\Category::where('parent_id',$menu1->id)->get();
                              $demc1 = count($demc);
                              ?>
                           @if($demc1 ==0)
                              <a class="nav-link dropdown-toggle" href="loai-tin/{{$menu1->id}}/{{$menu1->slug}}.html"  id="navbarDropdown" role="button" >
                              {{$menu1->name}}
                              </a>
                           @elseif($demb1 ==1)

                           @foreach($demb as $d)
                              <a class="nav-link dropdown-toggle" href="tin-tuc/{{$d->id}}/{{$d->slug}}.html"  id="navbarDropdown" role="button" >
                                 {{$menu1->name}} 
                                 </a>
                           @endforeach
                           @else
                           <a class="nav-link dropdown-toggle" href="tat-ca/{{$menu1->id}}/{{$menu1->slug}}.html"  id="navbarDropdown" role="button" >
                           {{$menu1->name}}
                           </a>
                           @endif
                           <?php 
                              $menu_level_2 = App\Category::where('parent_id',$menu1->id)->where('c_publish',1)->get();
                              ?>
                           <div class="dropdown-menu bg-light" aria-labelledby="navbarDropdown">
                              @foreach($menu_level_2 as $menu2)
                              <?php 
                                 $dem = App\NewCate::where('category_id',$menu2->id)->get();
                                 $dem1 = count($dem);
                                 ?>
                              @if($dem1 ==1)
                              @foreach($dem as $d)
                              <a class="dropdown-item" href="tin-tuc/{{$d->id}}/{{$d->slug}}.html">{{$menu2->name}}</a>
                              @endforeach
                              @else
                              <a class="dropdown-item" href="loai-tin/{{$menu2->id}}/{{$menu2->slug}}.html">{{$menu2->name}}</a>
                              @endif
                              @endforeach
                           </div>
                        </li>
                        @endforeach
                        
                       {{--  <li class="nav-item">
                           <a class="nav-link" href="{{route('blog')}}">Blog</a>
                        </li> --}}
                        <li class="nav-item">
                           <a class="nav-link" href="{{route('lienhe')}}">Liên hệ</a>
                        </li>
                       
                        {{--! Thay đổi ngôn ngữ--}}
                         
                        {{-- <li class="language">
                           <a href="{{URL::asset('')}}locale/vi" class="nav-link" ><img src="theme_page/images/tiengviet.png" style="height: 28px;"></a>
                        </li>
                        <li class="language">
                           <a href="{{URL::asset('')}}locale/en" class="nav-link"><img src="theme_page/images/tienganh.png" style="height: 20px;"></a>
                        </li>
                        <li class="language">
                           <a href="{{URL::asset('')}}locale/japan" class="nav-link"><img src="theme_page/images/tiengnhat.png" style="height: 20px;"></a>
                        </li> --}}
                       
                       
                     </ul>
                  </div>
               </div>
            </nav>


         </div>
      </div>
   </div>
</div>

<div style="clear: both;"></div>
