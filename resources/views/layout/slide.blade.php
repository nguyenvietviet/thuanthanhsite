 {{-- <section class="home-slider owl-carousel"> --}}

{{-- <?php
  $slide = App\Slide::all();
 ?>  
@foreach($slide as $sl)

      <div class="slider-item" style="background-image:url(image_slide/{{$sl->hinh}});">
        <div class="container">
          <div class="row no-gutters slider-text align-items-center justify-content-start" data-scrollax-parent="true">
        
        </div>
        </div>
      </div>
@endforeach
    </section> --}}

<?php
  $slide = App\Slide::select('hinh')->get();
 ?>  


<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    @foreach($slide as $sl)

    <div class="carousel-item @if($loop->first) active @endif">
      <img class="d-block w-100" src="image_slide/{{$sl->hinh}}">
    </div>
    
    @endforeach
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>




