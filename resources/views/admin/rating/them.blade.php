@extends('admin.layouts.index')
@section('content')

<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Đánh giá</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              {{-- <li class="breadcrumb-item">Trang chủ</li> --}}
              <li class="breadcrumb-item active">Đánh giá</li>
              <li class="breadcrumb-item active"><a href="{{route('admin.get.list.rating')}}">Danh sách</a></li>
              <li class="breadcrumb-item active">Thêm mới</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
<div class="animated fadeIn">
   <div class="row">
      <div class="col-md-12">
         <div class="card card-info">
            <div class="card-header">
               <strong class="card-title">Thêm Rating</strong>
            </div>
            <div class="card-body">
              
               <form action="admin/rating/them" method="POST" enctype="multipart/form-data">
                  {{-- <input type="hidden" name="_token" value="{{csrf_token()}}" /> --}}
                  @csrf
                            <div class="form-group">
                                <label>Author</label>
                                <input class="form-control" name="author" placeholder="Nhập vào tác giả" />
                            </div>
                 
                            <div class="form-group">
                                <label>Công ty:</label>
                                <input class="form-control" name="title" placeholder="Nhập vào chức vụ và cty" />
                            </div>
                
                            <div class="form-group">
                                <label>Nội dung</label>
                                <textarea name="content" id="content" class="form-control" cols="20" rows="5"></textarea>
                            </div>

                             <div class="form-group">
                                <label>Hình ảnh</label>
                                <input type="file" name="avatar" class="form-control" />
                            </div>
                            
                        
                  <button type="submit" class="btn btn-primary btn-sm">Thêm</button>
                  <button type="reset" class="btn btn-danger btn-sm">Làm mới</button>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
@endsection
{{-- @endsection

@section('js')
    <script>
        $(document).ready(function(){
            $("#categories").change(function(){
                var a_category_id_type = $(this).val();
                $.get("admin/ajax/typearticle/"+a_category_id_type,function(data){ 
                    $("#typearticle").html(data);   
                });
            });
        });
    </script>
    <script type="text/javascript">
       CKEDITOR.replace('content', {
          language:'vi',
          filebrowserBrowseUrl: 'http://localhost/thuanthanhtech/public/ckfinder/ckfinder.html',
          filebrowserUploadUrl: 'http://localhost/thuanthanhtech/public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
          filebrowserWindowWidth: '1000',
          filebrowserWindowHeight: '700'
      } );
    </script>
 @endsection --}}
