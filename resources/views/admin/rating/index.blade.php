@extends('admin.layouts.index')
@section('content')

 {{-- <li><a href="{{route('admin.get.list.rating')}}">Danh sách</a></li>
<li><a href="{{route('admin.get.create.rating')}}">Thêm</a></li> --}}

<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Đánh giá</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Trang chủ</a></li>
              <li class="breadcrumb-item active">Danh sách</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

<div class="animated fadeIn">

                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <strong class="card-title">Tin tức  <a href="{{route('admin.get.create.rating')}}" class="pull-right badge badge-warning"><i class="fas fa-plus-circle" aria-hidden="true"></i> Thêm mới</a></strong>
                        </div>
                        <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                  <thead>
                     <tr>
                        <th>STT</th>
                        <th>Author</th>
                        <th>Cty</th>
                        <th>Nội dung</th>
                        <th>Thao tác</th>
                     </tr>
                  </thead>
                  <?php $i=1 ?>
                  <tbody>
                     @foreach($rating as $ra)
                     <tr class="odd gradeX">
                        <td>{{$i++}}</td>
                        <td>{{$ra->r_author}}</td>
                         <td>
                           <p>{{$ra->r_title}}</p>
                           <img width="100px" src="rating/{{$ra->r_avatar}}"/>
                        </td>
                        <td>{!!$ra->r_content!!}</td>
                        <td>
                           <a href="{{route('admin.get.edit.rating',$ra->id)}}" class="badge badge-warning" style="padding:5px 10px;"><i class="fas fa-pen"></i> Sửa</a>
                           <a onclick="return confirm('Bạn có muốn xóa tin này?')" href="{{route('deleteRating',$ra->id)}}" class="badge badge-danger" style="padding:5px 10px;"><i class="fas fa-trash-alt"></i> Xóa</a>
                        </td>
                     </tr>
                     @endforeach
                  </tbody>
               </table>
               <?php $i++ ?>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection