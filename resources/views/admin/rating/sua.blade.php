@extends('admin.layouts.index')
@section('content')
<div class="breadcrumbs">
   <div class="col-sm-4">
      <div class="page-header float-left">
         <div class="page-title">
            <h1>Rating</h1>
         </div>
      </div>
   </div>
   <div class="col-sm-8">
      <div class="page-header float-right">
         <div class="page-title">
            <ol class="breadcrumb text-right">
               <li class="active">Rating</a></li>
               <li><a href="{{route('admin.get.list.rating')}}">Danh sách</a></li>
               <li><a href="{{route('admin.get.create.rating')}}">Thêm</a></li>
            </ol>
         </div>
      </div>
   </div>
</div>
<div class="animated fadeIn">
   <div class="row">
      <div class="col-md-12">
         <div class="card">
            <div class="card-header">
               <strong class="card-title">Sửa Rating</strong>
            </div>
            <div class="card-body">
              
               <form action="admin/rating/sua/{{$rating->id}}" method="POST" enctype="multipart/form-data">
                  {{-- <input type="hidden" name="_token" value="{{csrf_token()}}" /> --}}
                  @csrf
                  <div class="form-group">
                     <label>Author</label>
                     <input class="form-control" name="author" placeholder="Nhập vào tác giả" value="{{$rating->r_author}}" />
                  </div>
            
                  <div class="form-group">
                     <label>Công ty</label>
                     <input class="form-control" name="title" placeholder="Nhập vào chức vụ trong cty" value="{{$rating->r_title}}" />
                  </div>
                  
                  <div class="form-group">
                     <label>Nội dung</label>
                     <textarea name="content" id="content" class="form-control" cols="20" rows="5">{{$rating->r_content}}</textarea>
                  </div>

                  <div class="form-group">
                     <label>Hình ảnh</label>
                     <p>
                        <img height="100px;" src="rating/{{$rating->r_avatar}}">
                     </p>
                     <input type="file" name="avatar" class="form-control"/>
                  </div>
      
                  <button type="submit" class="btn btn-primary btn-sm">Update</button>
                  <button type="reset" class="btn btn-danger btn-sm">Làm mới</button>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
@endsection
