@extends('admin.layouts.index')
@section('title')
	Tin tức
@endsection

@section('content')
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Tin tức</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              {{-- <li class="breadcrumb-item">Trang chủ</li> --}}
              <li class="breadcrumb-item active">Tin tức</li>
              {{-- <li class="breadcrumb-item active"><a href="{{route('new.list')}}">Danh sách chờ duyệt</a></li> --}}
              <li class="breadcrumb-item active">Sửa</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
<div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card card-warning">
                        <div class="card-header">
                            <strong class="card-title">Sửa tin</strong>
                        </div>
                        <div class="card-body">

               <form action="admin/news/sua/{{$news->id}}" method="POST" enctype="multipart/form-data">
                
                  @csrf
              <div class="col-md-12">
                
                 @if (session('loi'))
                   <div class="alert  alert-danger alert-dismissible fade show" style="width:350px;" role="alert">
                    <span class="badge badge-pill badge-danger">{{ session('loi') }}</span> 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  @endif
                  <style>
                    .error-text{
                      color:red;
                    }
                  </style>

                  @if (session('thongbao'))
                  <div class="alert  alert-success alert-dismissible fade show" style="width:250px;" role="alert">
                    <span class="badge badge-pill badge-success">{{ session('thongbao') }}</span> 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  @endif        
          <div class="row">
                <div class="col-md-6">
                          <div class="form-group">
                             <label>Chọn danh mục</label>
                             <select class="form-control" id="category_id" name="category_id">
                                <option value="0">--Danh mục gốc--</option>
                                <?php cate_parent($cate,0,"",$news["category_id"]); ?>
                            </select>
                           </div>
                           
                            <div class="form-group">
                                <label>Title</label>
                                <input class="form-control" name="title" placeholder="Nhập vào tiêu đề" value="{{$news->title}}" />
                            </div>

                            <div class="form-group">
                              <label>Tác giả</label>
                              <input type="text" name="author" value="{{$news->author}}" class="form-control">
                          </div>
                
                            <div class="form-group">
                                <label>Tóm tắt</label>
                                <textarea name="tomtat" id="ckeditor" class="form-control ckeditor" rows="3" value="">{{$news->tomtat}}</textarea>
                            </div>
                            
                            
                  </div>

                  <div class="col-md-6">
                          {{-- <div class="form-group">
                              <label>Ngày ban hành</label>
                              <input type="date" name="ngaybanhanh" value="{{$news->ngaybanhanh}}" class="form-control">
                          </div>
                           <div class="form-group">
                              <label>Ngày hiệu lực</label>
                              <input type="date" name="ngayhieuluc" value="{{$news->ngayhieuluc}}" class="form-control">
                          </div> --}}
                          <div class="form-group">
                                <label>Nội dung</label>
                                <textarea name="content" id="content" class="form-control ckeditor" rows="5">{{$news->content}}</textarea>
                            </div>

                          <div class="form-group">
                                <label>Icon</label>
                                <p>
                                  <img height="100px;" src="upload/{{$news->icon}}">
                               </p>
                                <input type="file" name="icon" class="form-control" />
                          </div>
                          
                          <div class="form-group">
                                <label>Hình ảnh</label>
                                <p>
                                  <img height="100px;" src="upload/{{$news->image}}">
                               </p>
                                <input type="file" name="image" class="form-control" />
                          </div>
                          <div class="form-group">
                                <label>File đính kèm</label>
                                <input type="file" name="file_document" value="{{$news->file_document}}" class="form-control" />
                          </div>
{{--                             <div class="form-group">
                                <label>Nổi bật</label>
                                <label class="radio-inline">
                                    <input name="n_hot" value="0" type="radio">Không
                                </label>
                                <label class="radio-inline">
                                    <input name="n_hot" value="1" type="radio">Có
                                </label>
                            </div> --}}
                           <button type="submit" class="btn btn-primary btn-sm">Cập nhật</button>
                           <button type="reset" class="btn btn-danger btn-sm">Làm mới</button>
                         </div>
                       </div>
               </form> 
                  </div>
                  
                  </div>
                  
            </div>
                    </div>
                </div>


                </div>
    </div>

@endsection