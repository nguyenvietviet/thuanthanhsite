@extends('admin.layouts.index')
@section('title')
	Tin tức
@endsection

@section('content')
<style>
  .dinhkem a{
    color: blue;
  }
  .dinhkem a:hover{
    color:#b5b5ec;
  }
</style>

<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Tin tức</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Trang chủ</a></li>
              <li class="breadcrumb-item active">Danh sách</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

<div class="animated fadeIn">

                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <strong class="card-title">Tin tức  <a href="{{route('new.create')}}" class="pull-right badge badge-warning"><i class="fas fa-plus-circle" aria-hidden="true"></i> Thêm mới</a></strong>
                        </div>
                        <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>STT</th>
                        <th>Tiêu đề</th>
                        {{-- <th>Tóm tắt</th> --}}
                        <th>Danh mục</th>
                        <th>Special</th>
                        <th>Văn bản-Chuyên mục</th>
                        <th>Trạng thái</th>
                        <th>Nổi bật</th>
                        <th>Dowload</th>
                        <th>Thao tác</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($news as $key => $new)
                      <tr>
                        <td>{{$key+1}}</td>
                        <td>
                           <p>{{$new->title}}</p>
                           <img width="100px" src="upload/{{$new->image}}"/>
                        </td>
                        {{-- <td>{!!$new->tomtat!!}</td> --}}
                        <td>{{$new->category->name}}</td>
                        <td>
                         <a href="{{route('deleteNew',['special_slide',$new->id])}}" class="badge {{$new->getSpecial($new->special)['class']}}">{{$new ->getSpecial($new->special)['name']}}</a>
                        </td>
                        <td>
                          <a href="{{route('deleteNew',['type',$new->id])}}" class="badge {{$new->getType($new->newType)['class']}}">{{$new ->getType($new->newType)['name']}}</a>
                        </td>
                        <td>
                           <a href="{{route('deleteNew',['publish',$new->id])}}" class="badge {{$new->getStatus($new->n_publish)['class']}}">{{$new ->getStatus($new->n_publish)['name']}}</a>
                        </td>
                        <td>
                           <a href="{{route('deleteNew',['hot',$new->id])}}" class="badge {{$new->getHot($new->n_hot)['class']}}">{{$new ->getHot($new->n_hot)['name']}}</a>
                        </td>
                        <td class="dinhkem">
                          <!--Down load file_ducument-->
                          <a href="file_document/{{$new->file_document}}">
                            {{-- <button type="button" class="btn btn-primary">
                              <i class="fas fa-download">
                                {{$new->file_document}}
                              </i>
                            </button> --}}
                            {{$new->file_document}}
                          </a>
                        </td>
                        <td>
                          <a href="{{route('new.edit',$new->id)}}" class="badge badge-warning" style="padding:5px 10px;"><i class="fas fa-pen"></i> Sửa</a>
                          <a onclick="return confirm('Bạn có muốn xóa?')" href="{{route('deleteNew',['delete',$new->id])}}" class="badge badge-danger" style="padding:5px 10px;"><i class="fas fa-trash-alt"></i> Xóa</a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                        </div>
                    </div>
                </div>

    </div>

@endsection