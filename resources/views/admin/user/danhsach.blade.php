@extends('admin.layouts.index')
@section('content')



<div class="modal" id="myModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Thêm môn học</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
    <form action="{{route('user.insert')}}" method="POST" enctype="multipart/form-data">
                        
              @csrf
    <div class="col-md-12">
      <!-- Modal body -->
      <div class="modal-body">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Nhập tên tài khoản</label>
                    <input type="text" name="name" class="form-control" placeholder="Nhập tên">
                </div>

                <div class="form-group">
                    <label>Email:</label>
                    <input type="email" name="email" class="form-control" placeholder="Nhập email" >
                </div>

                <div class="form-group">
                    <label>SĐT:</label>
                    <input type="number" name="phone" class="form-control" placeholder="Nhập phone">
                </div>

                <div class="form-group">
                    <label>Mật khẩu:</label>      
                    <input type="Password" name="password" class="form-control"  placeholder="Nhập số tiết">
                </div>

              </div>

              <div class="col-md-6">

                <div class="form-group">
                    <label>PostOffice:</label>
                    <input type="text" name="postoffice" class="form-control" placeholder="Nhập địa chỉ văn phòng làm việc">
                </div>

                <div class="form-group">
                    <label>Địa chỉ:</label>
                    <input type="text" name="address" class="form-control" placeholder="Nhập địa chỉ">
                </div>

                <div class="form-group">
                    <label>Chứng minh nhân dân:</label>
                    <input type="number" name="address" class="form-control" placeholder="Nhập số cmnd">
                </div>


                <div class="form-group">
                    <label>Nhập lại Password:</label>
                    <input type="Password" name="passwordAgain" class="form-control"/>
                </div>

                <div class="form-group">
                    <label>Quyền người dùng:</label>
                    <br>
                    <label class="radio-inline">
                        <input name="quyen" value="0" checked="" type="radio">Thường
                    </label>
                    <label class="radio-inline">
                        <input name="quyen" value="1" type="radio">Admin
                    </label>
                </div>
              </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Thêm</button>
        <button type="reset" class="btn btn-danger">Làm mới</button>
      </div>

      </div>
  </form>

    </div>
  </div>
</div>

<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Tài khoản</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Tài khoản</li>
              <li class="breadcrumb-item active"><a href="{{route('user.list')}}">Danh sách</a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>


  <div class="animated fadeIn">

                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-header">
                                <strong class="card-title">Danh sách tài khoản  <a href="{{route('user.create')}}" class="pull-right badge badge-warning"><i class="fas fa-plus-circle" aria-hidden="true"></i> Thêm mới</a></strong>
                        </div>

                        
                        <div class="card-body">

                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>STT</th>
                        <th>Tên</th>
                        <th>Email</th>
                        <th>Level</th>
                        <th>PostOffice</th>
                        <th>Thao tác</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($users as $key => $u)
                      <tr>
                        <td>{{$key+1}}</td>
                        <td>
                          <p>{{$u->name}}</p>
                          <img width="50px" src="avatar/{{$u->avatar}}" alt="avatar" />
                        </td>
                        <td>{{$u->email}}</td>
                        <td>
                            @if($u->quyen==1)
                            {{"Admin"}}
                            @else
                            {{"Thường"}}
                            @endif
                        </td>
                        <td>{{$u->postoffice}}</td>
                        <td>
                              <a href="{{route('user.edit',$u->id)}}" class="badge badge-warning" style="padding:5px 10px;"><i class="fas fa-pen"></i> Sửa</a>
                              <a onclick="return confirm('Bạn có muốn xóa?')" href="{{route('deleteUser',$u->id)}}" class="badge badge-danger" style="padding:5px 10px;"><i class="fas fa-trash-alt"></i> Xóa</a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                        </div>
                    </div>
                </div>


            </div>
@endsection