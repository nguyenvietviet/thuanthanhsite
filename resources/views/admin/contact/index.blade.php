@extends('admin.layouts.index')
@section('content')

<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Liên hệ</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Trang chủ</a></li>
              <li class="breadcrumb-item active">Danh sách</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>


 <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <strong class="card-title">Quản lý liên hệ</strong>

                        </div>
                        <div class="card-body">
                 <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>STT</th>
                        <th>Tên</th>
                        <th>Tiêu đề</th>
                        <th>Email</th>
                        <th>SĐT</th>
                        <th>Địa chỉ</th>
                        <th>Nội dung</th>  
                        <th>Ngày</th>
                        <th>Trạng thái</th>  
                        <th>Thao tác</th>
                      </tr>
                    </thead>
                    <?php $i=1 ?>
                    <tbody>
                      @foreach($contact as $ct)
                      <tr>
                        <td>{{$i++}}</td>
                        <td>{{$ct->c_name}}</td>
                        <td>{!!$ct->c_title!!}</td>
                        <td>{{$ct->c_email}}</td>
                        <td>{{$ct->c_phone}}</td>
                        <td>{{$ct->c_address}}</td>
                        <td>{{$ct->c_content}}</td>
                        <th>{{$ct->created_at}}</th>
                        <td>
                            <a href="{{route('deleteCt',['active',$ct->id])}}" class="badge {{$ct->getStatus($ct->c_status)['class']}}">{{$ct ->getStatus($ct->c_status)['name']}}</a>
                        </td>
                        <td>
                          <a onclick="return confirm('Bạn có muốn xóa?')" href="{{route('deleteCt',['delete',$ct->id])}}" class="badge badge-danger" style="padding:5px 10px;"><i class="fas fa-trash-alt"></i> Xóa</a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                   <?php $i++ ?>
                        </div>
                    </div>
                </div>


                </div>
            </div>
@endsection