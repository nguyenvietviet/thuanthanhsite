{{-- <!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>

    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
     <base href="{{asset('')}}">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="theme_admin/images/logo_40.png">

    <link rel="stylesheet" href="theme_admin/assets/css/normalize.css">
    <link rel="stylesheet" href="theme_admin/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="theme_admin/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="theme_admin/assets/css/themify-icons.css">
    <link rel="stylesheet" href="theme_admin/assets/css/flag-icon.min.css">
    <link rel="stylesheet" href="theme_admin/assets/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="theme_admin/assets/css/lib/datatable/dataTables.bootstrap.min.css">
    <!-- <link rel="stylesheet" href="theme_admin/assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="theme_admin/assets/scss/style.css">
    <script src="https://kit.fontawesome.com/c60f6dddbd.js" crossorigin="anonymous"></script>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
    <style>
        .error-text {
            color: red;
            font-style: italic;
            font-size: 14px;
        }
    </style>

</head>

<body>
        <!-- Left Panel -->
        @include('admin.layouts.menu')
   <!-- /#left-panel -->

    <!-- Left Panel -->

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel">

        <!-- Header-->
        @include('admin.layouts.header')
        <!-- Header-->


        @yield('content')<!-- .content -->


    </div><!-- /#right-panel -->

    <!-- Right Panel -->


    <script src="theme_admin/assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="theme_admin/assets/js/popper.min.js"></script>
    <script src="theme_admin/assets/js/plugins.js"></script>
    <script src="theme_admin/assets/js/main.js"></script>


    <script src="theme_admin/assets/js/lib/data-table/datatables.min.js"></script>
    <script src="theme_admin/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
    <script src="theme_admin/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
    <script src="theme_admin/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
    <script src="theme_admin/assets/js/lib/data-table/jszip.min.js"></script>
    <script src="theme_admin/assets/js/lib/data-table/pdfmake.min.js"></script>
    <script src="theme_admin/assets/js/lib/data-table/vfs_fonts.js"></script>
    <script src="theme_admin/assets/js/lib/data-table/buttons.html5.min.js"></script>
    <script src="theme_admin/assets/js/lib/data-table/buttons.print.min.js"></script>
    <script src="theme_admin/assets/js/lib/data-table/buttons.colVis.min.js"></script>
    <script src="theme_admin/assets/js/lib/data-table/datatables-init.js"></script>


    <script type="text/javascript">
        $(document).ready(function() {
          $('#bootstrap-data-table-export').DataTable();
        } );
    </script>

    <script src="ckeditor/ckeditor.js"></script>
    <script src="ckfinder/ckfinder.js"></script>
    @yield('js')

</body>
</html>
 --}}




 <!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Thuận thành tech</title>
  <!-- Tell the browser to be responsive to screen width -->
  <base href="{{asset("")}}">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="shortcut icon" href="theme_admin/images/logo_40.png">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="theme_admin1/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="theme_admin1/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="theme_admin1/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <script src="https://kit.fontawesome.com/c60f6dddbd.js" crossorigin="anonymous"></script>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{route('home')}}" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li>
    </ul>

    <ul class="navbar-nav ml-auto">
     
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#">
          <i class="fas fa-th-large"></i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('home')}}" class="brand-link">
      <img src="theme_admin/images/logo_40.png"
           alt="Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Thuận Thành Tech</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
    @if(Auth::check())
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="theme_admin1/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="admin/user/sua/{{Auth::user()->id}}" class="d-block"></i>{{Auth::user()->name}}</a>
          <a href="admin/logout" class="d-block">Thoát</a>
        </div>
      </div>
    @endif
      <!-- Sidebar Menu -->
      @include('admin.layouts.menu')
      
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

<section class="content">
    @yield('content')
</section>

    
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      {{-- <b>Version</b> 3.0.2 --}}
    </div>
    <strong>Copyright © 2020 by   <a href="thuanthanhtech.com">Thuanthanhtech</a> </strong>Co., Ltd. All rights reserved
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="theme_admin1/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="theme_admin1/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables -->
<script src="theme_admin1/plugins/datatables/jquery.dataTables.js"></script>
<script src="theme_admin1/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<!-- AdminLTE App -->
<script src="theme_admin1/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="theme_admin1/dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>

 <script src="ckeditor/ckeditor.js"></script>
<script src="ckfinder/ckfinder.js"></script>
   
       

    <script type="text/javascript">
        $(document).ready(function() {
          $('#bootstrap-data-table-export').DataTable();
        } );
    </script>
    <script>
        function readURL(input) {
              if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function(e) {
                  $('#out_img').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]);
              }
            }
            $("#input_img").change(function() {
              readURL(this);
            });
    </script>

@yield('js')
</body>
</html>
