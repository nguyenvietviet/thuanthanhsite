{{--  <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href=""><img src="theme_admin/images/logo.png" alt="Logo"></a>
                <a class="navbar-brand hidden" href="./"><img src="theme_admin/images/logo2.png" alt="Logo"></a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li>
                        <a href=""> <i class="menu-icon fa fa-dashboard"></i>Trang chủ </a>
                    </li>
                 
                    <li class=" {{\Request::route()->getName()=='category.list' ? 'active' : ''}}">
                        <a href="{{route('category.list')}}"> <i class="menu-icon fa fa-tasks"></i>Danh mục</a>
                    </li>

                    <li class=" {{\Request::route()->getName()=='new.list' ? 'active' : ''}}">
                        <a href="{{route('new.list')}}"> <i class="menu-icon fa fa-bars"></i>Tin tức</a>
                    </li>

                    <li class="{{\Request::route()->getName()=='slide.list' ? 'active' : ''}}">
                        <a href="{{route('slide.list')}}"> <i class="menu-icon fa fa-book"></i>Slide</a>
                    </li>

                    <li class="{{\Request::route()->getName()=='user.list' ? 'active' : ''}}">
                        <a href="{{route('user.list')}}"> <i class="menu-icon fa fa-id-card-o"></i>Tài khoản</a>
                    </li>


                    <li class="{{\Request::route()->getName()=='admin.get.list.about' ? 'active' : ''}}">
                        <a href="{{route('admin.get.list.about')}}"> <i class="menu-icon fa fa-puzzle-piece"></i>About</a>
                    </li>

                    <li class="{{\Request::route()->getName()=='admin.get.list.rating' ? 'active' : ''}}">
                        <a href="{{route('admin.get.list.rating')}}"> <i class="menu-icon fa fa-comments-o"></i>Đánh giá</a>
                    </li>

                     <li class="{{\Request::route()->getName()=='admin.get.list.contact' ? 'active' : ''}}">
                        <a href="{{route('admin.get.list.contact')}}"> <i class="menu-icon fa fa-spinner"></i>Contact</a>
                    </li>

                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside> --}}


<nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="{{route('home')}}" class="nav-link {{\Request::route()->getName()=='home' ? 'active' : ''}}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Trang tổng quan
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{route('category.list')}}" class="nav-link {{\Request::route()->getName()=='category.list' ? 'active' : ''}}">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Danh mục
              </p>
              <i class="right fas fa-angle-left"></i>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('category.list')}}" class="nav-link {{\Request::route()->getName()=='category.list' ? 'active' : ''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('category.create')}}" class="nav-link {{\Request::route()->getName()=='category.create' ? 'active' : ''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm mới</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Tin tức
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>

            <ul class="nav nav-treeview">

             <li class="nav-item">
                <a href="{{route('new.list')}}" class="nav-link {{\Request::route()->getName()=='new.list' ? 'active' : ''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('new.create')}}" class="nav-link {{\Request::route()->getName()=='new.create' ? 'active' : ''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm mới</p>
                </a>
              </li>
            </ul>
          </li>
  

                 
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-images"></i>
              <p>
                Slide
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('slide.list')}}" class="nav-link {{\Request::route()->getName()=='slide.list' ? 'active' : ''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('slide.create')}}" class="nav-link {{\Request::route()->getName()=='slide.create' ? 'active' : ''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm mới</p>
                </a>
              </li>
              
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="{{route('user.list')}}" class="nav-link {{\Request::route()->getName()=='user.list' ? 'active' : ''}}">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Tài khoản
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('user.list')}}" class="nav-link {{\Request::route()->getName()=='user.list' ? 'active' : ''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('user.create')}}" class="nav-link {{\Request::route()->getName()=='user.create' ? 'active' : ''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm mới</p>
                </a>
              </li>
            </ul>
          </li>


          <li class="nav-item has-treeview">
            <a href="{{route('admin.get.list.about')}}" class="nav-link {{\Request::route()->getName()=='admin.get.list.about' ? 'active' : ''}}">
              <i class="nav-icon fas fa-book"></i>
              <p>
                About
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('admin.get.list.about')}}" class="nav-link {{\Request::route()->getName()=='admin.get.list.about' ? 'active' : ''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.get.create.about')}}" class="nav-link {{\Request::route()->getName()=='admin.get.create.about' ? 'active' : ''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm mới</p>
                </a>
              </li>
            </ul>
          </li>


          <li class="nav-item has-treeview">
            <a href="{{route('admin.get.list.rating')}}" class="nav-link {{\Request::route()->getName()=='admin.get.list.rating' ? 'active' : ''}}">
              <i class="nav-icon fas fa-star"></i>
              <p>
                Đánh giá
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('admin.get.list.rating')}}" class="nav-link {{\Request::route()->getName()=='admin.get.list.rating' ? 'active' : ''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('admin.get.create.rating')}}" class="nav-link {{\Request::route()->getName()=='user.create' ? 'active' : ''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm mới</p>
                </a>
              </li>
            </ul>
          </li>


          <li class="nav-item has-treeview">
            <a href="{{route('admin.get.list.contact')}}" class="nav-link {{\Request::route()->getName()=='admin.get.list.contact' ? 'active' : ''}}">
              <i class="nav-icon fas fa-id-card-alt"></i>
              <p>
                Contact
                {{-- <i class="fas fa-angle-left right"></i> --}}
              </p>
            </a>
            
          </li>
         
         
         
         
            </ul>
      </nav>