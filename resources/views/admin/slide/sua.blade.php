@extends('admin.layouts.index')
@section('content')
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Slide</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Slide</a></li>
              <li class="breadcrumb-item active"><a href="{{route('slide.list')}}">Danh sách</a></li>
              <li class="breadcrumb-item active">Sửa</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
<div class="animated fadeIn">
   <div class="row">
      <div class="col-md-12">
         <div class="card card-warning">
            <div class="card-header">
               <strong class="card-title">Sửa slide</strong>
            </div>
            <div class="card-body">
               
                <form action="admin/slide/sua/{{$slide->id}}" method="POST" enctype="multipart/form-data">
                      @csrf

                @if (session('loi'))
                   <div class="alert  alert-danger alert-dismissible fade show" style="width:350px;" role="alert">
                    <span class="badge badge-pill badge-danger">{{ session('loi') }}</span> 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                @endif
                
                  
                  @if (session('thongbao'))
                    <div class="alert  alert-success alert-dismissible fade show" style="width:250px;" role="alert">
                      <span class="badge badge-pill badge-success">{{ session('thongbao') }}</span> 
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                  @endif      
                      <div class="form-group">
                          <label>Tên</label>
                          <input class="form-control" name="Ten" value="{{$slide->ten}}" placeholder="Nhập vào tiêu đề" />
                      </div>
          
                      <div class="form-group">
                          <label>Nội dung</label>
                          <textarea name="NoiDung" id="ckeditor" class="form-control ckeditor" rows="3">{{$slide->noidung}}</textarea>
                      </div>
                     
                      <div class="form-group">
                          <label>Hình ảnh</label>
                           <p>
                              <img height="200px" src="image_slide/{{$slide->hinh}}">
                           </p>
                           <input type="file" name="Hinh" class="form-control"/>
                      </div>
                  
                      <button type="submit" class="btn btn-primary btn-sm">Thêm</button>
                      <button type="reset" class="btn btn-danger btn-sm">Làm mới</button>
                  <form>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
@endsection