@extends('admin.layouts.index')
@section('content')
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">About</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a>Trang chủ</a></li>
              <li class="breadcrumb-item"><a href="{{route('admin.get.list.about')}}">Danh sách</a></li>
              <li class="breadcrumb-item">Thêm mới</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

<div class="animated fadeIn">
      <div class="col-md-12">
         <div class="card card-info">
            <div class="card-header">
               <strong class="card-title">Thêm About</strong>
            </div>

            <div class="card-body">


        <style>
              .error-text{
                color:red;
              }
            </style>

      @if (session('thongbao'))
               <div class="alert  alert-success alert-dismissible fade show" style="width:250px;float: right;" role="alert">
                  <span class="badge badge-pill badge-success">{{ session('thongbao') }}</span> 
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
               </div>
      @endif    
              
               <form action="admin/about/them" method="POST" enctype="multipart/form-data">
                  @csrf
                 
                            <div class="form-group">
                                <label>Title</label>
                                <input class="form-control" name="title" placeholder="Nhập vào tiêu đề" />
                            </div>
                
                            <div class="form-group">
                                <label>Nội dung</label>
                                <textarea name="content" id="content" class="form-control ckeditor" rows="5"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Hình ảnh</label>
                                <input type="file" name="avatar" class="form-control" />
                            </div>
                        
                  <button type="submit" class="btn btn-primary btn-sm">Thêm</button>
                  <button type="reset" class="btn btn-danger btn-sm">Làm mới</button>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
@endsection

@section('js')
    <script>
        $(document).ready(function(){
            $("#categories").change(function(){
                var a_category_id_type = $(this).val();
                $.get("admin/ajax/typearticle/"+a_category_id_type,function(data){ 
                    $("#typearticle").html(data);   
                });
            });
        });
    </script>
    <script type="text/javascript">
       CKEDITOR.replace('content', {
          language:'vi',
      } );
    </script>
 @endsection
