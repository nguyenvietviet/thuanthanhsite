@extends('admin.layouts.index')
@section('content')
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">About</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a>Trang chủ</a></li>
              <li class="breadcrumb-item"><a href="{{route('admin.get.list.about')}}">Danh sách</a></li>
              <li class="breadcrumb-item">Sửa</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

<div class="animated fadeIn">
      <div class="col-md-12">
         <div class="card card-warning">
            <div class="card-header">
               <strong class="card-title">Sửa About</strong>
            </div>
            <div class="card-body">

          <style>
              .error-text{
                color:red;
              }
            </style>

      @if (session('thongbao'))
               <div class="alert  alert-success alert-dismissible fade show" style="width:250px;float: right;" role="alert">
                  <span class="badge badge-pill badge-success">{{ session('thongbao') }}</span> 
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
               </div>
      @endif        
              
               <form action="admin/about/sua/{{$about->id}}" method="POST" enctype="multipart/form-data">
                  @csrf
            
                  <div class="form-group">
                     <label>Tiêu Đề</label>
                     <input class="form-control" name="title" placeholder="Nhập vào tiêu đề" value="{{$about->about_title}}" />
                  </div>
                  
                  <div class="form-group">
                     <label>Nội dung</label>
                     <textarea name="content" id="content" class="form-control ckeditor" rows="5">{{$about->about_content}}</textarea>
                  </div>
                  <div class="form-group">
                     <label>Hình ảnh</label>
                     <p>
                        <img height="100px;" src="about/{{$about->about_avatar}}">
                     </p>
                     <input type="file" name="avatar" class="form-control"/>
                  </div>
      
                  <button type="submit" class="btn btn-primary btn-sm">Update</button>
                  <button type="reset" class="btn btn-danger btn-sm">Làm mới</button>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
@endsection
@section('js')

<script type="text/javascript">
   CKEDITOR.replace('content', {
      language:'vi',
   } );
</script>
@endsection