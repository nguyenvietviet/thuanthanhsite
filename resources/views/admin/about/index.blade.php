@extends('admin.layouts.index')
@section('content')

<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">About</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Trang chủ</a></li>
              <li class="breadcrumb-item active">Danh sách</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>


 <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <strong class="card-title">Quản lý About </strong>

                        </div>
                        <div class="card-body">
                 <table id="example1" class="table table-bordered table-striped">
                  <thead>
                     <tr>
                        <th>STT</th>
                        <th>Tiêu đề</th>
                        <th>Nội dung</th>
                        <th>Thao tác</th>
                     </tr>
                  </thead>
                  <?php $i=1 ?>
                  <tbody>
                     @foreach($about as $ab)
                     <tr class="odd gradeX">
                        <td>{{$i++}}</td>

                        <td>
                           <p>{{$ab->about_title}}</p>
                           <img height="70px" src="about/{{$ab->about_avatar}}"/>
                        </td>
                        <td style="text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;">{!!$ab->about_content!!}</td>
                        <td>
                           <a href="{{route('admin.get.edit.about',$ab->id)}}" class="badge badge-warning" style="padding:5px 10px;"><i class="fas fa-pen"></i> Sửa</a>
                           <a onclick="return confirm('Bạn có muốn xóa tin này?')" href="{{route('deleteAb',$ab->id)}}" class="badge badge-danger" style="padding:5px 10px;"><i class="fas fa-trash-alt"></i> Xóa</a>
                        </td>
                     </tr>
                     @endforeach
                  </tbody>
               </table>
               <?php $i++ ?>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection