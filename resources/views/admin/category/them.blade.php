@extends('admin.layouts.index')
@section('content')
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Danh mục</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a>Trang chủ</a></li>
              <li class="breadcrumb-item"><a href="{{route('category.list')}}">Danh sách</a></li>
              <li class="breadcrumb-item">Thêm mới</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

<div class="animated fadeIn">
      <div class="col-md-12">
         <div class="card card-info">
            <div class="card-header">
               <strong class="card-title">Thêm danh mục</strong>
            </div>

            <div class="card-body">


        <style>
              .error-text{
                color:red;
              }
            </style>

      @if (session('thongbao'))
               <div class="alert  alert-success alert-dismissible fade show" style="width:250px;float: right;" role="alert">
                  <span class="badge badge-pill badge-success">{{ session('thongbao') }}</span> 
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
               </div>
      @endif     
               <form action="admin/category/them" method="POST">
                  {{-- <input type="hidden" name="_token" value="{{csrf_token()}}" /> --}}
                  @csrf
                     
                     <div class="form-group">
                       <label>Chọn danh mục</label>
                       <select class="form-control" id="parent_id" name="parent_id">
                          <option value="0">--Danh mục gốc--</option>
                         {{--  @foreach($categories as $cate)
                          <option value="{{$cate->id}}">{{$cate->name}}</option>
                          @endforeach --}}
                          <?php cate_parent($parent); ?>
                      </select>

                      
                     </div>

                     <div class="form-group">
                        <label>Tên Danh mục</label>
                        <input class="form-control" name="name" placeholder="nhập tên danh mục" />

                        @if($errors->has('name'))
                          <span class="error-text">
                          {{$errors->first('name')}}
                          </span>
                        @endif
                     </div>
                     
                     <div class="form-group">
                        <label>Số thứ tự Sắp xếp Trên menu</label>
                        <input class="form-control" type="number" name="thutu" placeholder="nhập số thứ tự" />
                     </div>
                     <button type="submit" class="btn btn-primary btn-sm">Thêm</button>
                     <button type="reset" class="btn btn-danger btn-sm">Làm mới</button>
               </form>
            </div>
         </div>
      </div>
</div>
@endsection