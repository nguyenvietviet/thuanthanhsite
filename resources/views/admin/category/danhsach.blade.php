@extends('admin.layouts.index')
@section('title')
	Danh mục
@endsection

@section('content')
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Danh mục</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Trang chủ</a></li>
              <li class="breadcrumb-item active">Danh sách</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>


 <div class="animated fadeIn">
                <div class="row">

                <div class="col-md-12">
                    <div class="card card-info">
                        <div class="card-header">
                            <strong class="card-title">Quản lý danh mục </strong>

                        </div>
                        <div class="card-body">
                 <table id="example1" class="table table-bordered table-striped">

                    <thead>
                      <tr>
                        <th>STT</th>
                        <th>Danh mục</th>
                        <th>ParentId</th>
                        <th>Sắp xếp</th>
                        <th>ShowIndex</th>
                        <th>Trạng thái</th>
                        <th>Nổi bật</th>
                        <th>Thao tác</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($categories as $key => $category)
                      <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$category->name}}</td>
                        <td>
                            @if($category->parent_id == 0)
                            {{"None"}}
                            @else
                            <?php 
                                $parent = DB::table('categories')->where('id',$category->parent_id)->first();
                                echo $parent->name;
                            ?>
                            @endif
                        </td>
                        <td>{{$category->thutu}}</td>
                        <td>
                            <a href="{{route('deleteCate',['show',$category->id])}}" class="badge {{$category->getShowindex($category->show_index)['class']}}">{{$category ->getShowindex($category->show_index)['name']}}</a>
                        </td>
                        <td>
                           <a href="{{route('deleteCate',['publish',$category->id])}}" class="badge {{$category->getStatus($category->c_publish)['class']}}">{{$category ->getStatus($category->c_publish)['name']}}</a>
                        </td>
                        <td>
                           <a href="{{route('deleteCate',['hot',$category->id])}}" class="badge {{$category->getHot($category->c_hot)['class']}}">{{$category ->getHot($category->c_hot)['name']}}</a>
                        </td>
                        <td>
                          <a href="{{route('category.edit',$category->id)}}" class="badge badge-warning" style="padding:5px 10px;"><i class="fas fa-pen"></i> Sửa</a>
                          <a onclick="return confirm('Bạn có muốn xóa?')" href="{{route('deleteCate',['delete',$category->id])}}" class="badge badge-danger" style="padding:5px 10px;"><i class="fas fa-trash-alt"></i> Xóa</a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                        </div>
                    </div>
                </div>


                </div>
    </div>

@endsection