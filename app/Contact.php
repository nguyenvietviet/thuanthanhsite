<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Contact extends Model
{
    //
    protected $table = 'contacts';

    protected $fillable = ['c_name','c_email','c_address','c_phone','c_title','c_content'];

    const STATUS_PUBLIC =1;
    const STATUS_PRIVATE =0;


    protected $status=[
    	0=>[
    		'name' => 'Chờ xử lý',
    		'class' => 'badge-dark'
    	],
    	1=>[
    		'name' => 'Đã xử lý',
    		'class' => 'badge-success'
    	]
    ];

    public function getStatus()
    {
    	return Arr::get($this->status,$this->c_status,'[N\A]');
    }
}
