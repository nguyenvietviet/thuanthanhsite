<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class NewCate extends Model
{
    //
    protected $table ="news";
    protected $guarded =[''];

    const STATUS_PUBLIC =1;
    const STATUS_PRIVATE =0;

    const HOT_ON =1;
    const HOT_OFF =0;

    const TYPE_ON =1;
    const TYPE_OFF =0;

    const SPECIAL_ON =1;
    const SPECIAL_OFF =0;

    protected $status=[
        1=>[
            'name' => 'Hiển thị',
            'class' => 'badge-success'
        ],
        0=>[
            'name' => 'Không hiển thị',
            'class' => 'badge-secondary'
        ]
    ];
     protected $hot=[
        1=>[
            'name' => 'Nổi bật',
            'class' => 'badge-success'
        ],
        0=>[
            'name' => 'Không',
            'class' => 'badge-secondary'
        ]
    ];

     protected $type=[
        1=>[
            'name' => 'OFF',
            'class' => 'badge-secondary'
        ],
        0=>[
            'name' => 'ON',
            'class' => 'badge-success'
        ]
    ];

    protected $special_slide=[
        0=>[
            'name' => 'OFF',
            'class' => 'badge-secondary'
        ],
        1=>[
            'name' => 'ON',
            'class' => 'badge-success'
        ]
    ];

    public function getStatus()
    {
        return Arr::get($this->status,$this->n_publish,'[N\A]');
    }
    public function getHot()
    {
        return Arr::get($this->hot,$this->n_hot,'[N\A]');
    }

    public function getType()
    {
        return Arr::get($this->type,$this->newType,'[N\A]');
    }

    public function getSpecial()
    {
        return Arr::get($this->special_slide,$this->special,'[N\A]');
    }


    public function new()
    {
    	return $this->hasMany('App\NewCate','category_id','id');
    }

    public function category()
    {
        return $this->belongsTo('App\Category','category_id','id');
    }
}
