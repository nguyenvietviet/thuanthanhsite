<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;
use App\About;

class AboutController extends Controller
{
    //
    public function index()
	{
		// $article = Article::get();
		$about = About::get();
		return view('admin.about.index',['about'=>$about]);
	} 

	public function create()
	{
		$about = About::all();

		return view('admin.about.them',['about'=>$about]);
	}

	public function store(Request $request)
	{
		$this->validate($request,
			[
				
				'title'=>'required|min:3|unique:abouts,about_title',
				'content'=>'required'
			],
			[
			
				'title.required'=>'Bạn chưa nhập tiêu đề',
				'title.min'=>'Tiêu đề phải có ít nhất 3 ký tự',
				'title.unique'=>'Tiêu đề đã tồn tại',
				'content.required'=>'Bạn chưa nhập nội dung'
			]
		);

		$about = new About;
		$about->about_title = $request->title;
		$about->about_content = $request->content;
		$about->about_slug = str::slug($request->title,'-');

		if($request->hasFile('avatar')){

		$file = $request->file('avatar')->getClientOriginalName();
		$about->about_avatar = $file;
        $request->file('avatar')->move(base_path('public/about/'),$file);
    	}else{
    		$about->about_avatar = '';
    	}
	
        $about->save();
       return redirect('admin/about/them')->with('thongbao','Thêm thành công');

	}   

	//Sửa
	public function edit($id)
	{
		
		$about = About::find($id);

		return view('admin.about.sua',['about'=>$about]);
	}

	public function update(Request $request,$id)
	{
		// $this->validate($request,
		// 	[
				
		// 		'title'=>'required|min:3',
		// 		'content'=>'required',
		// 	],
		// 	[
			
		// 		'title.required'=>'Bạn chưa nhập tiêu đề',
		// 		'title.min'=>'Tiêu đề phải có ít nhất 3 ký tự',
				
		// 		'content.required'=>'Bạn chưa nhập nội dung',
		// 	]
		// );

		$about = About::find($id);
		$about->about_title = $request->title;
		$about->about_content = $request->content;
		$about->about_slug = Str::slug($request->title,'-');

		// $file = $request->file('avatar')->getClientOriginalName();
		// $about->about_avatar = $file;
  //       $request->file('avatar')->move(base_path('public/about/'),$file);

        if($request->hasFile('avatar'))
		{
			$file = $request->file('avatar');

			$name = $file->getClientOriginalName();
			$Hinh = Str::random(2)."_".$name;

			while(file_exists("about/".$Hinh))
			{
				$Hinh = Str::random(2)."_".$name;
			}

			$file->move("about",$Hinh);
			if($about->about_avatar)
			{
				unlink("about/".$about->about_avatar);
			}
			$about->about_avatar = $Hinh;
		}
		
	
        $about->save();

       return redirect('admin/about/sua/'.$about->id)->with('thongbao','Sửa thành công');
	}


	//Xóa
	public function getXoa($id)
    {
        About::where('id',$id)->delete();
        return redirect()->back()->with('thongbao','Xóa thành công');
    }
	
}
