<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Slide;

class AdminSlideController extends Controller
{
    public function index()
    {
        $slide = Slide::get();
        return view('admin.slide.danhsach',['slide'=>$slide]);
    }

    public function create()
    {
        return view('admin.slide.them');
    }

    public function store(Request $request)
    {
        $this->validate($request,[

            'Hinh'=>'max:3000'
        ],
        [
            
            'Hinh.max' =>'Dung lượng ảnh vượt quá 3M'
        ]);

        $slide = new Slide;
        $slide->ten = $request->Ten;
        $slide->noidung = $request->NoiDung;
        
        $file = $request->file('Hinh')->getClientOriginalName();
        $slide->hinh = $file;
        $request->file('Hinh')->move(base_path('public/image_slide/'),$file);

        $slide->save();
        return redirect('admin/slide/them')->with('thongbao','Thêm thành công');

    }

    public function edit($id)
    {
        $slide = Slide::find($id);
        return view('admin.slide.sua',['slide'=>$slide]);
    }

    public function update(Request $request,$id)
    {

        $slide = Slide::find($id);
        $slide->ten = $request->Ten;
        $slide->noidung = $request->NoiDung;
        
        if($request->hasFile('Hinh'))
        {
            $file = $request->file('Hinh');

            $name = $file->getClientOriginalName();
            $Hinh = Str::random(2)."_".$name;

            while(file_exists("image_slide/".$Hinh))
            {
                $Hinh = Str::random(2)."_".$name;
            }

            $file->move("image_slide",$Hinh);
            if($slide->hinh)
            {
                unlink("image_slide/".$slide->hinh);
            }
            $slide->hinh = $Hinh;
        }

        $slide->save();
        return redirect('admin/slide/sua/'.$slide->id)->with('thongbao','Sửa thành công');
    }

    public function getXoa($id)
    {
        $slide = Slide::find($id);

        if(file_exists('image_slide/'.$slide->hinh) && $slide->hinh !=NULL)
            unlink('image_slide/'.$slide->hinh);
        
        $slide->delete($slide->id);

        return redirect()->back();
    }
}
