<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Rating;

class RatingController extends Controller
{
    public function index()
	{
		// $article = Article::get();
		$rating = Rating::get();
		return view('admin.rating.index',['rating'=>$rating]);
	} 

	public function create()
	{
		$rating = Rating::all();

		return view('admin.rating.them',['rating'=>$rating]);
	}

	public function store(Request $request)
	{
		$this->validate($request,
			[
				
				'title'=>'required|min:3|unique:ratings,r_title',
				'content'=>'required',
				
			],
			[
			
				'title.required'=>'Bạn chưa nhập tiêu đề',
				'title.min'=>'Tiêu đề phải có ít nhất 3 ký tự',
				'title.unique'=>'Tiêu đề đã tồn tại',
				'content.required'=>'Bạn chưa nhập nội dung',
				
			]
		);

		$rating = new Rating;
		$rating->r_title = $request->title;
		$rating->r_author = $request->author;
		$rating->r_content = $request->content;

		if($request->hasFile('avatar')){

		$file = $request->file('avatar')->getClientOriginalName();
		$rating->r_avatar = $file;
        $request->file('avatar')->move(base_path('public/rating/'),$file);
    	}else{
    		$rating->r_avatar = '';
    	}

        $rating->save();
       return redirect('admin/rating/them')->with('thongbao','Thêm thành công');

	}   

	//Sửa
	public function edit($id)
	{
		
		$rating = Rating::find($id);

		return view('admin.rating.sua',['rating'=>$rating]);
	}

	public function update(Request $request,$id)
	{

		$rating = Rating::find($id);
		$rating->r_title = $request->title;
		$rating->r_author = $request->author;
		$rating->r_content = $request->content;


		if($request->hasFile('avatar'))
		{
			$file = $request->file('avatar');

			$name = $file->getClientOriginalName();
			$Hinh = Str::random(2)."_".$name;

			while(file_exists("rating/".$Hinh))
			{
				$Hinh = Str::random(2)."_".$name;
			}

			$file->move("rating",$Hinh);
			if($rating->r_avatar)
			{
				unlink("rating/".$rating->r_avatar);
			}
			$rating->r_avatar = $Hinh;
		}

        $rating->save();
       return redirect('admin/rating/sua/'.$rating->id)->with('thongbao','Sửa thành công');
	}


	//Xóa
	public function getXoa($id)
    {
        Rating::where('id',$id)->delete();
        return redirect()->back()->with('thongbao','Xóa thành công');
    }
	
}
