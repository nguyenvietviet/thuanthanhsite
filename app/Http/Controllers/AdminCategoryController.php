<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Category;
use App\NewCate;

class AdminCategoryController extends Controller
{
    //
    public function index()
    {
    	$categories = Category::all();
    	return view('admin.category.danhsach',['categories'=>$categories]);
    }

    public function create()
    {
    	$parent = Category::all();
    	return view('admin.category.them',['parent'=>$parent]);
    }

    public function store(Request $request)
    {
        $this-> validate($request ,
            [
                'name' => 'required|unique:categories,name|min:3|max:100'
            ],
            [
                'name.unique'=> 'danh mục đã tồn tại',
                'name.required'=>'Bạn chưa nhập danh mục',
                'name.min'=>'Tên danh mục phải có độ dài từ 3 cho đến 100 ký tự',
                'name.max'=>'Tên danh mục phải có độ dài từ 3 cho đến 100 ký tự',
            ]);

    	$categories = new Category;
    	$categories->name = $request->name;
        $categories->slug = str::slug($request->name);
    	$categories->thutu = $request->thutu;
    	$categories->parent_id = $request->parent_id;

    	$categories->save();

    	return redirect('admin/category/them')->with('thongbao','Thêm thành công');
    }

    public function edit($id)
    {
        $data = Category::find($id);
        $parent = Category::all();
        return view('admin.category.sua',['data'=>$data,'parent'=>$parent]);
    }

    public function update(Request $request,$id)
    {
        $categories = Category::find($id);
        $categories->name = $request->name;
        $categories->slug = str::slug($request->name);
        $categories->thutu = $request->thutu;
        $categories->parent_id = $request->parent_id;

        $categories->save();

        return redirect('admin/category/sua/'.$categories->id)->with('thongbao','Sửa thành công');

    }

    public function getXoa($getXoa,$id)
    {

        if($getXoa)
        {
            $categories = Category::find($id);
            $parent = Category::where('parent_id',$id)->select('id','parent_id','c_publish','c_hot')->count();
            $news = NewCate::where('category_id',$id)->select('id','category_id','image','file_document','tomtat','content','title','slug','newType','n_hot','n_publish','author','coquan','thutu','ngaybanhanh','ngayhieuluc','created_at','updated_at')->get();
            
            switch($getXoa)
            {
                case 'delete':
                    if($parent == 0)
                    {
                        $categories = Category::find($id);
                        if($categories->delete($id))
                        {
                            foreach ($news as $new) {

                                //Xóa hết ảnh và file của table news
                                        if(file_exists('upload/'.$new->image) && $new->image !=NULL)
                                            unlink('upload/'.$new->image);
                                        

                                        if(file_exists('file_document/'.$new->file_document) && $new->file_document !=NULL)
                                            unlink('file_document/'.$new->file_document);

                                        $new->delete($new->id);

                                        // $new->delete($new->id);

                            }
                        }
                    }else{
                        echo" <script type='text/javascript'>
                            aleft('Xin lỗi ! bạn không thể xóa Danh mục này');
                            window.location = '";
                            echo route('category.list');
                        echo"'
                        </script>";
                    }
                    break;

                case 'publish':
                     $categories->c_publish = $categories->c_publish ? 0:1;
                     $categories->save();
                    break;
             
                 case 'hot':
                     $categories->c_hot = $categories->c_hot ? 0:1;
                     $categories->save();
                    break;

                case 'show':
                     $categories->show_index = $categories->show_index ? 0:1;
                     $categories->save();
                    break;        
            }
           
        }
        return redirect()->back();
    }
}
