<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;
use App\Category;
use App\Slide;
use App\Contact;

class ContactController extends HomeController
{
 //    function __construct()
	// {
	// 	$categories = Category::orderBy('sapxep','ASC')->get();
	// 	$slide = Slide::all();
	// 	view()->share('categories',$categories);
	// 	view()->share('slide',$slide);
	// }
    
	public function index()
	{
		$contact = Contact::all();
		return view('admin.contact.index',['contact'=>$contact]);
	}

	//Xóa Contact
	public function action($getXoa,$id)
    {
        if($getXoa)
        {
            $contact = Contact::find($id);
            switch($getXoa)
            {
                case 'delete':
                    $contact->delete();
                    break;

                case 'active':
                     $contact->c_status = $contact->c_status ? 0:1;
                     $contact->save();
                    break;
            }
        }
        return redirect()->back();
    }

    //Front-End Contact
	function getContact()
	{
		return view('pages.lienhe');
	}
	function saveContact(Request $request)
	{

		$data = $request->except('_token');
    	$data['created_at'] = $data['updated_at'] = Carbon::now();
       
    	Contact::insert($data);
    	return redirect()->back();
	}

}
