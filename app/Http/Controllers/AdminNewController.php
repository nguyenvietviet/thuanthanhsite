<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Category;
use App\NewCate;

class AdminNewController extends Controller
{
    public function index()
    {
    	$news = NewCate::with('category')->orderBy('created_at','DESC')->get();
    	return view('admin.news.danhsach',['news'=>$news]);
    }

    public function create()
    {
    	$cate = Category::all();
    	return view('admin.news.them',['cate'=>$cate]);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'category_id' => 'required',
            'title' => 'required|min:3|max:150',
            // 'tomtat' => 'required|min:3',
            'content' => 'required|min:3',
        ],
        [
            'category_id.required' => 'Bạn chưa chọn danh mục!',
            'title.required' => 'Tiêu đề không được để trống!',
            'title.min' =>  'Tiêu đề phải có độ dài từ 3 đến 150 ký tự!',
            'title.max' =>  'Tiêu đề phải có độ dài từ 3 đến 150 ký tự!',
            // 'tomtat.required' => 'Tóm tắt không được để trống!',
            // 'tomtat.min' => 'Tóm tắt phải có độ dài ít nhất là 3 ký tự!',
            'content.required'  =>  'Nội dung không được bỏ trống!',
            'content.min'   =>  'Nội dung phải có độ dài ít nhất là 3 ký tự'

        ]);

    	$news = new NewCate;

    	$news->category_id = $request->category_id;
    	$news->title = $request->title;
    	$news->tomtat = $request->tomtat;
    	$news->slug = str::slug($request->title);
    	$news->content = $request->content;
    	$news->ngaybanhanh = $request->ngaybanhanh;
    	$news->ngayhieuluc = $request->ngayhieuluc;
    	$news->author = $request->author;

    	if($request->hasFile('image')){
    		$file = $request->file('image')->getClientOriginalName();
			$news->image = $file;
        	$request->file('image')->move(base_path('public/upload/'),$file);
    	}else{

    		$news->image = '';
    	}

        if($request->hasFile('icon')){
            $file = $request->file('icon')->getClientOriginalName();
            $news->icon = $file;
            $request->file('icon')->move(base_path('public/upload/'),$file);
        }else{

            $news->image = '';
        }
    	
    	if($request->hasFile('file_document')){
	        $fileD = $request->file('file_document')->getClientOriginalName();
			$news->file_document = $fileD;
	        $request->file('file_document')->move(base_path('public/file_document/'),$fileD);
        }else{

    		$news->file_document = '';
    	}
        // dd($news);
	
        $news->save();

        return redirect('admin/news/them')->with('thongbao','Thêm thành công');
    }

    public function edit($id)
    {
       	$cate = Category::all();
		$news = NewCate::find($id);		

    	return view('admin.news.sua',compact('cate','news'));
    }

    public function update(Request $request,$id)
    {
    	$news = NewCate::find($id);

    	$news->category_id = $request->category_id;
    	$news->title = $request->title;
    	$news->tomtat = $request->tomtat;
    	$news->slug = str::slug($request->title);
    	$news->content = $request->content;
    	$news->ngaybanhanh = $request->ngaybanhanh;
    	$news->ngayhieuluc = $request->ngayhieuluc;
    	$news->author = $request->author;

    	if($request->hasFile('image'))
		{
			$file = $request->file('image');

			$name = $file->getClientOriginalName();
			$Hinh = Str::random(2)."_".$name;

			while(file_exists("upload/".$Hinh))
			{
				$Hinh = Str::random(2)."_".$name;
			}

			$file->move("upload",$Hinh);
			if($news->image)
			{
				unlink("upload/".$news->image);
			}
			$news->image = $Hinh;
		}


        if($request->hasFile('icon'))
        {
            $fileI = $request->file('icon');

            $name = $fileI->getClientOriginalName();
            $Icon = Str::random(2)."_".$name;

            while(file_exists("upload/".$Icon))
            {
                $Icon = Str::random(2)."_".$name;
            }

            $fileI->move("upload",$Icon);
            if($news->icon)
            {
                unlink("upload/".$news->icon);
            }
            $news->icon = $Icon;
        }

		if($request->hasFile('file_document'))
		{
			$fileD = $request->file('file_document');

			$name = $fileD->getClientOriginalName();
			$Ducument = Str::random(2)."_".$name;

			while(file_exists("file_document/".$Ducument))
			{
				$Ducument = Str::random(2)."_".$name;
			}

			$fileD->move("file_document",$Ducument);
			if($news->file_document)
			{
				unlink("file_document/".$news->file_document);
			}
			$news->file_document = $Ducument;
		}

		$news->save();

		return redirect('admin/news/sua/'.$news->id)->with('thongbao','Sửa thành công');
	
	
    }

    public function getXoa($getXoa,$id)
    {
    	if($getXoa)
        {
            $news = NewCate::find($id);
            switch($getXoa)
            {
                case 'delete':
                   if(file_exists('upload/'.$news->image) && $news->image !=NULL)
                            unlink('upload/'.$news->image);
                   if(file_exists('upload/'.$news->icon) && $news->icon !=NULL)
                            unlink('upload/'.$news->icon);
                    if(file_exists('file_document/'.$news->file_document) && $news->file_document !=NULL)
                            unlink('file_document/'.$news->file_document);
                        $news->delete($news->id);
                    break;
                case 'publish':
                    $news->n_publish = $news->n_publish ? 0:1;
                     $news->save();
                    break;
                case 'hot':
                    $news->n_hot = $news->n_hot ? 0:1;
                     $news->save();
                    break;
                case 'type':
                    $news->newType = $news->newType ? 0:1;
                     $news->save();
                    break;

                case 'special_slide':
                    $news->special = $news->special ? 0:1;
                    $news->save();
                    break;
            }
        }
        return redirect()->back();
    }
}
