<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\User;

class UserController extends Controller
{
    //
    public function index()
    {
    	$users = User::all();

    	return view('admin.user.danhsach',['users'=>$users]);
    }

    public function create()
    {
        return view('admin.user.them');
    }

    
    public function store(Request $request)
    {

        // $this->validate($request,[
        //     'name' => 'required',
        //     'email' => 'required|unique:users,email',
        //     'password' => 'required'
        // ],
        // [
        //     'name.required' => 'Bạn chưa nhập tên',
        //     'email.required' => 'Bạn chưa nhập email',
        //     'email.unique'  => 'Email đã tồn tại',
        //     'password.required'  => 'Bạn chưa nhập mật khẩu'

        // ]);
        $this->validate($request,
            [
                'name'=>'required|min:3',
                'email'=>'required|email|unique:users,email',
                'password'=>'required|min:3|max:33',
                'passwordAgain'=>'required|same:password'
            ],
            [
                'name.required'=>'Bạn chưa nhập tên',
                'name.min'=>'Tên người dùng phải có ít nhất 3 ký tự',
                'email.required'=>'Bạn chưa nhập Email',
                'email.email'=>'Bạn chưa nhập đúng định dạng email',
                'email.unique'=>'Email đã tồn tại',
                'password.required'=>'Bạn chưa nhập password',
                'password.min'=>'Mật khẩu phải có ít nhất 3 ký tự',
                'password.max'=>'Mật khẩu có tối đa là 33 ký tự',
                'passwordAgain.required'=>'Bạn chưa nhập lại mật khẩu',
                'passwordAgain.same'=>'Mật khẩu nhập lại chưa khớp'
            ]);

            $users = new User;
            $users->name = $request->name;
            $users->email = $request->email;
            $users->password = bcrypt($request->password);
            $users->quyen = $request->quyen;
            $users->address = $request->address;
            $users->phone = $request->phone;
            $users->cmnd   = $request->cmnd;
            $users->postoffice = $request->postoffice;

            if($request->hasFile('avatar')){
            $file = $request->file('avatar')->getClientOriginalName();
            $users->avatar = $file;
            $request->file('avatar')->move(base_path('public/avatar/'),$file);
            }else{

                $users->avatar = '';
            }

            // dd($users);
            $users->save();

            return back()->with('thongbao','Thêm tài khoản thành công');
    }


    public function edit($id)
    {
        $users = User::find($id);

        return view('admin.user.sua',['users'=>$users]);
    }


    public function update(Request $request,$id)
    {
        $this->validate($request,
            [
                'name'=>'required|min:3',
                'email'=>'required|email',
                'password'=>'required|min:3',
                'passwordAgain'=>'required|same:password'
            ],
            [
                'name.required'=>'Bạn chưa nhập tên',
                'name.min'=>'Tên người dùng phải có ít nhất 3 ký tự',
                'email.required'=>'Bạn chưa nhập Email',
                'email.email'=>'Bạn chưa nhập đúng định dạng email',
                // 'email.unique'=>'Email đã tồn tại',
                'password.required'=>'Bạn chưa nhập password',
                'password.min'=>'Mật khẩu phải có ít nhất 3 ký tự',
                'passwordAgain.required'=>'Bạn chưa nhập lại mật khẩu',
                'passwordAgain.same'=>'Mật khẩu nhập lại chưa khớp'
            ]);

            $users = User::find($id);
            $users->name = $request->name;
            $users->email = $request->email;
            $users->password = bcrypt($request->password);
            $users->quyen = $request->quyen;
            $users->address = $request->address;
            $users->phone = $request->phone;
            $users->cmnd   = $request->cmnd;
            $users->postoffice = $request->postoffice;

            if($request->hasFile('avatar'))
        {
            $file = $request->file('avatar');

            $name = $file->getClientOriginalName();
            $Hinh = Str::random(2)."_".$name;

            while(file_exists("avatar/".$Hinh))
            {
                $Hinh = Str::random(2)."_".$name;
            }

            $file->move("avatar",$Hinh);
            if($users->avatar)
            {
                unlink("avatar/".$users->avatar);
            }
            $users->avatar = $Hinh;
        }

            // dd($users);
            $users->save();

            return back()->with('thongbao','Thêm tài khoản thành công');
    }


    public function getXoa($id)
    {

        $users = User::find($id);
    	if(file_exists('avatar/'.$users->avatar) && $users->avatar !=NULL)
            unlink('avatar/'.$users->avatar);
        $users->delete($users->id);

        return back()->with('thongbao','Xóa thành công');
    }

    public function getDangnhapAdmin()
    {
        return view('admin.login');
    }

    public function postDangnhapAdmin(Request $request)
    {
        $this ->validate($request,[
            'email'=> 'required',
            'password'=>'required|min:3|max:32'

        ],
        [
            'email.required' =>'Bạn chưa nhập email',
            'password.required' =>'bạn chưa nhập password',
            'password.min'=>'Mật khẩu phải có ít nhất 3 ký tự',
            'password.max'=>'Mật khẩu có tối đa là 33 ký tự',

        ]);
        if(Auth::attempt(['email'=>$request->email,'password'=>$request->password]))
        {
            return redirect('admin/category/danhsach');
        }else
        {
            return redirect('admin/dangnhap')->with('thongbao','Đăng nhập thất bại');
        }
        
    }

    public function DangXuatAdmin()
    {
        Auth::logout();

        return redirect('admin/dangnhap');
    }
}
