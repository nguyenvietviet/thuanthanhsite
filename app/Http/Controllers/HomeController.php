<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Slide;
use App\NewCate;
use App\About;
use App\Rating;

class HomeController extends Controller
{


    public function trangchu()
    {
    	// $news = NewCate::orderBy('created_at','ASC')->where('newType',1)->orWhere('n_publish')->limit(1)->get();
    	$newSlide = NewCate::orderBy('created_at','ASC')->where('special',1)->limit(3)->get();

        // $slide = Slide::all();
        $about = About::orderBy('created_at','DESC')->limit(6)->get();

    	$newHot = NewCate::orderBy('created_at','ASC')->where('n_hot',1)->limit(3)->get();

    	// $newTypes= NewCate::orderBy('created_at','ASC')->where('newType',0)->limit(5)->get();

    	// return view('pages.trangchu',['news'=>$news,'newSlide'=>$newSlide,'newHot'=>$newHot,'newTypes'=>$newTypes,'about'=>$about]);
        return view('pages.trangchu',['newSlide'=>$newSlide,'newHot'=>$newHot,'about'=>$about]);
    }

    //Loại tin

    public function loaitin($id)
    {
    	$categories = Category::find($id);

    	$news = NewCate::where('category_id',$id)->paginate(3);

    	return view('pages.loaitin',['categories'=>$categories,'news'=>$news]);
    }

    //Chi tiết tin tức

    public function tintuc($id)
    {
    	$categories = Category::find($id);
    	$news = NewCate::find($id);

        //Đánh giá 
        // $rating = Rating::get();

        $newHot = NewCate::orderBy('created_at','ASC')->where('special',1)->limit(3)->get();

        //Lấy tin tức liên quan
        $newsN = NewCate::where('category_id',$news->category_id)->limit(6)->get();

        return view('pages.tintuc',['categories'=>$categories,'news'=>$news,'newsN'=>$newsN,'newHot'=>$newHot]);
    }


    //Tìm kiếm theo title
    public function timkiem(Request $request)
    {

        $tukhoa = $request->k;
        
             $news = NewCate::where('title','like',"%$tukhoa%")->orWhere('tomtat','like',"%$tukhoa%");

             $news = $news->paginate(4);
          

        return view('pages.search',['news'=>$news]);
    }

    
    //Lấy tất cả danh sách tin tức của Danh mục gốc
    public function tatca($id)
    {
        $categories = Category::find($id);
        $parent = Category::where('parent_id',$id)->select('id','name')->get();

        // dd($parent);
         
         // foreach($parent as $pa)
         // {
         //    $news = NewCate::where('category_id',$pa->id)->select('id','category_id','image','file_document','tomtat','content','title','slug','newType','n_hot','n_publish','author','coquan','thutu','ngaybanhanh','ngayhieuluc','created_at','updated_at')->get();
         //    // dd($new);

         // }

        return view('pages.tatca',['parent'=>$parent,'categories'=>$categories]);

    }

    public function blog()
    {
        $articleBlog = NewCate::orderBy('id','DESC')->paginate(6);

        return view('pages.blog',['articleBlog'=>$articleBlog]);
    }
}
