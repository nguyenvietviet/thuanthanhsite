<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Category extends Model
{
    //
    protected $table ="categories";
    protected $guarded =[''];

    const STATUS_PUBLIC =1;
    const STATUS_PRIVATE =0;

    const HOT_ON =1;
    const HOT_OFF =0;

    const SHOW_ON = 1;
    const SHOW_OFF = 0;

    protected $status=[
        1=>[
            'name' => 'Hiển thị',
            'class' => 'badge-danger'
        ],
        0=>[
            'name' => 'Private',
            'class' => 'badge-secondary'
        ]
    ];
     protected $hot=[
        1=>[
            'name' => 'Không hiển thị',
            'class' => 'badge-success'
        ],
        0=>[
            'name' => 'Không',
            'class' => 'badge-secondary'
        ]
    ];

    protected $show=[
        1=>[
            'name' => 'Show',
            'class' => 'badge-success'
        ],
        0=>[
            'name' => 'Private',
            'class' => 'badge-secondary'
        ]
    ];

    public function getStatus()
    {
        return Arr::get($this->status,$this->c_publish,'[N\A]');
    }
    public function getHot()
    {
        return Arr::get($this->hot,$this->c_hot,'[N\A]');
    }

    public function getShowindex()
    {
        return Arr::get($this->show,$this->show_index,'[N\A]');
    }



    public function new()
    {
    	return $this->hasMany('App\NewCate','category_id','id');
    }
}
