<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->id();
            $table->integer('category_id');
            $table->string('name');
            $table->unsignedInteger('user_id')->nullable();
            $table->string('lang_id',10)->nullable();
            $table->string('title')->nullable();
            $table->string('slug')->nullable();
            $table->longText('tomtat')->nullable();
            $table->longText('content')->nullable();    
            $table->string('image')->nullable();
            $table->string('file_document')->nullable();
            $table->tinyInteger('n_hot')->default(0);
            $table->tinyInteger('newType')->default(1);
            $table->tinyInteger('n_publish')->default(0);
            $table->string('author')->nullable();
            $table->string('coquan')->nullable();
            $table->string('linhvu')->nullable();
            $table->string('hinhthucvb')->nullable();
            $table->integer('thutu')->default(0);
            $table->date('ngayhieuluc')->nullable();
            $table->date('ngaybanhanh')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
