<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->integer('parent_id')->default(0);
            $table->string('lang_id',10)->nullable();
            $table->string('image')->nullable();
            $table->string('tomtat')->nullable();
            $table->string('title')->nullable();
            $table->integer('thutu')->nullable();
            $table->tinyInteger('level')->default(0);
            $table->tinyInteger('type')->default(1);
            $table->string('keywords')->nullable();
            $table->string('description')->nullable();
            $table->tinyInteger('show_index')->default(0);
            $table->tinyInteger('c_publish')->default(1);
            $table->tinyInteger('c_hot')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
