-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 24, 2020 at 04:36 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cpt_thuanthanh`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `about_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_content` text COLLATE utf8mb4_unicode_ci,
  `about_slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `about_avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `about_title`, `about_content`, `about_slug`, `about_avatar`, `created_at`, `updated_at`) VALUES
(2, 'Uy tín, chất lượng', '<p>Với hơn 4 năm được th&agrave;nh lập, ch&uacute;ng t&ocirc;i lu&ocirc;n đặt chữ t&iacute;n l&ecirc;n h&agrave;ng đầu, lu&ocirc;n tạo ra những sản phẩm chất lượng</p>', 'uy-tin-chat-luong', 'handshake.png', '2020-03-10 09:17:21', '2020-03-10 09:17:21'),
(3, 'Mô hình làm việc ưu việt', '<p>C&aacute;c nh&acirc;n&nbsp;vi&ecirc;n tại thuanthanhtech lu&ocirc;n hiểu được cốt l&otilde;i v&agrave; bản chất của lập tr&igrave;nh&nbsp;l&agrave; l&agrave;m ra sản phẩm tốt nhất cho kh&aacute;ch h&agrave;ng&hellip;</p>', 'mo-hinh-lam-viec-uu-viet', 'sD_bars.png', '2020-03-10 09:19:21', '2020-03-24 07:22:12');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `lang_id` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tomtat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thutu` int(11) DEFAULT NULL,
  `level` tinyint(4) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL DEFAULT '1',
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `show_index` tinyint(4) NOT NULL DEFAULT '0',
  `c_publish` tinyint(4) NOT NULL DEFAULT '1',
  `c_hot` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `parent_id`, `lang_id`, `image`, `tomtat`, `title`, `thutu`, `level`, `type`, `keywords`, `description`, `show_index`, `c_publish`, `c_hot`, `created_at`, `updated_at`) VALUES
(1, 'DỊCH VỤ', 'dich-vu', 0, 'vi', NULL, 'TIN TỨC - SỰ KIỆN', 'TIN TỨC - SỰ KIỆN', 2, 0, 1, NULL, NULL, 0, 1, 0, NULL, '2020-03-23 01:37:10'),
(9, 'Nội dung số', 'noi-dung-so', 1, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, 0, 1, 0, '2020-03-07 00:39:41', '2020-03-23 01:38:40'),
(13, 'Gia công phần mềm', 'gia-cong-phan-mem', 1, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, 0, 1, 0, '2020-03-07 00:41:12', '2020-03-12 08:29:20'),
(26, 'VỀ CHÚNG TÔI', 've-chung-toi', 0, NULL, NULL, NULL, NULL, 1, 0, 1, NULL, NULL, 0, 1, 0, '2020-03-09 09:14:40', '2020-03-12 08:25:44'),
(27, 'Giới thiệu chung', 'gioi-thieu-chung', 26, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, 0, 1, 0, '2020-03-09 09:15:22', '2020-03-09 09:15:35'),
(28, 'Giá trị cốt lõi', 'gia-tri-cot-loi', 26, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, 0, 1, 0, '2020-03-09 09:15:57', '2020-03-12 08:28:02'),
(30, 'Tầm nhìn - sứ mệnh', 'tam-nhin-su-menh', 26, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, 0, 1, 0, '2020-03-09 09:16:33', '2020-03-12 08:28:23'),
(35, 'GIẢI PHÁP & SẢN PHẨM', 'giai-phap-and-san-pham', 0, NULL, NULL, NULL, NULL, 3, 0, 1, NULL, NULL, 0, 1, 0, '2020-03-10 08:43:12', '2020-03-12 08:26:49'),
(36, 'TUYỂN DỤNG', 'tuyen-dung', 0, NULL, NULL, NULL, NULL, 4, 0, 1, NULL, NULL, 0, 1, 0, '2020-03-12 08:27:24', '2020-03-12 08:27:24'),
(38, 'Thuê ngoài nhân sự CNTT', 'thue-ngoai-nhan-su-cntt', 1, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, 0, 1, 0, '2020-03-12 08:30:18', '2020-03-12 08:30:18'),
(39, 'Sổ liên lạc điện tử', 'so-lien-lac-dien-tu', 35, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, 0, 1, 0, '2020-03-12 08:31:16', '2020-03-12 08:31:16'),
(40, 'Phần mềm quản lý bệnh viện', 'phan-mem-quan-ly-benh-vien', 35, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, 0, 1, 0, '2020-03-12 08:31:31', '2020-03-12 08:31:31'),
(41, 'ERP', 'erp', 35, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, 0, 1, 0, '2020-03-12 08:31:42', '2020-03-12 08:31:42');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `c_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_content` text COLLATE utf8mb4_unicode_ci,
  `c_address` text COLLATE utf8mb4_unicode_ci,
  `c_phone` int(11) DEFAULT NULL,
  `c_status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `c_name`, `c_title`, `c_email`, `c_content`, `c_address`, `c_phone`, `c_status`, `created_at`, `updated_at`) VALUES
(1, 'nguyễn việt', 'tiêu đề', 'hoa@gmail.com', NULL, 'Nguyễn Xiển', 9574383, 0, '2020-03-11 04:44:16', '2020-03-12 08:09:49');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(6, '2014_10_12_000000_create_users_table', 1),
(7, '2019_08_19_000000_create_failed_jobs_table', 1),
(8, '2020_03_04_045138_create_categories_table', 1),
(9, '2020_03_04_045203_create_news_table', 1),
(10, '2020_03_04_071139_create_slides_table', 1),
(11, '2020_03_09_094714_alter_column_special_in_news_table', 2),
(12, '2020_03_10_154906_create_ratings_table', 3),
(13, '2020_03_10_155047_create_abouts_table', 3),
(14, '2020_03_10_155213_create_contacts_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `lang_id` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tomtat` text COLLATE utf8mb4_unicode_ci,
  `content` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_document` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `newType` tinyint(4) NOT NULL DEFAULT '1',
  `n_hot` tinyint(4) NOT NULL DEFAULT '0',
  `n_publish` tinyint(4) NOT NULL DEFAULT '0',
  `author` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coquan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linhvu` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hinhthucvb` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thutu` int(11) NOT NULL DEFAULT '0',
  `ngayhieuluc` date DEFAULT NULL,
  `ngaybanhanh` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `special` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `category_id`, `user_id`, `lang_id`, `title`, `slug`, `tomtat`, `content`, `image`, `icon`, `file_document`, `newType`, `n_hot`, `n_publish`, `author`, `coquan`, `linhvu`, `hinhthucvb`, `thutu`, `ngayhieuluc`, `ngaybanhanh`, `created_at`, `updated_at`, `special`) VALUES
(1, 36, NULL, NULL, 'Tuyển dụng Android developer', 'tuyen-dung-android-developer', '<p>Tuyển dụng Android developer</p>', '<p>Tuyển dụng Android developer</p>', 'Aa_charlie-outsourcing-1080x675-1568537761840271101997.jpg', NULL, '', 1, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-03-19 14:49:28', '2020-03-20 02:21:41', 0),
(2, 36, NULL, NULL, 'eqwe', 'eqwe', '<p>Tuyển dụng Android developer</p>', '<p>Tuyển dụng Android developer</p>', 'Kr_11.jpg', NULL, '', 1, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-03-19 14:51:15', '2020-03-20 02:21:23', 0),
(3, 39, NULL, NULL, 'sổ liên lack', 'so-lien-lack', '<p>sổ li&ecirc;n lacksổ li&ecirc;n lacksổ li&ecirc;n lacksổ li&ecirc;n lack</p>', '<p>sổ li&ecirc;n lacksổ li&ecirc;n lacksổ li&ecirc;n lack</p>', '', NULL, '', 1, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-03-19 15:03:38', '2020-03-19 15:03:38', 0),
(4, 40, NULL, NULL, 'phần mềm bệnh viện', 'phan-mem-benh-vien', '<p>phần mềm bệnh viện</p>', '<p>phần mềm bệnh viện</p>', '', NULL, '', 1, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-03-19 15:04:00', '2020-03-19 15:04:00', 0),
(5, 27, NULL, NULL, 'Giới thiệu cty', 'gioi-thieu-cty', '<p>Giới thiệu cty</p>', '<p>Giới thiệu cty</p>', '', NULL, '', 1, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-03-19 15:04:18', '2020-03-19 15:04:18', 0),
(8, 27, NULL, NULL, 'file_document', 'file-document', '<p>file_document</p>', '<p>file_document</p>', '1.PNG', 'github.png', 'DC_KhoaLuan_NguyenVietViet (1).docx', 1, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-03-20 01:59:04', '2020-03-20 01:59:04', 0),
(9, 26, NULL, NULL, 'Về chúng tôi 12', 've-chung-toi-12', '<p>Về ch&uacute;ng t&ocirc;iVề ch&uacute;ng t&ocirc;iVề ch&uacute;ng t&ocirc;i</p>', '<p>Về ch&uacute;ng t&ocirc;iVề ch&uacute;ng t&ocirc;iVề ch&uacute;ng t&ocirc;i</p>', '', NULL, '', 1, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, '2020-03-20 03:08:45', '2020-03-20 03:08:45', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `r_content` text COLLATE utf8mb4_unicode_ci,
  `r_author` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `r_avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ratings`
--

INSERT INTO `ratings` (`id`, `r_content`, `r_author`, `r_title`, `r_avatar`, `created_at`, `updated_at`) VALUES
(1, 'Chúng tôi đã sử dụng dịch vụ của thuận thành tech , công ty đã đáp ứng được nguồn nhân lực về CNTT về chất lượng', 'Nguyễn Văn Kiên', 'Công ty TNHH Giải Pháp Công Nghệ Thuận Thành', 'person_2.jpg', '2020-03-10 09:27:04', '2020-03-11 04:52:48'),
(2, 'Chất lượng dịch vụ của công ty rất tốt , chúng tôi đã sử dụng dịch vụ của công ty , chúng tôi đánh giá cao các bạn với sự trách nhiệm trong công việc', 'Hoàng Khôi', 'Cty TNHH Nhập Khẩu Ánh Dương', 'person_4.jpg', '2020-03-11 04:50:13', '2020-03-11 04:50:13'),
(3, 'nhờ có thuận thành tech mà tập đoàn fpt chúng tôi có thêm được nhiều nhân viên về CNTT giỏi , cám ơn các bạn rất nhiều', 'Hoàng Nam Tiến', 'Chủ tịch Hội đồng quản trị FPT', 'photo-1-15834632715701996696754.png', '2020-03-11 04:51:07', '2020-03-11 04:51:07');

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ten` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `noidung` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hinh` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `ten`, `noidung`, `hinh`, `created_at`, `updated_at`) VALUES
(4, NULL, NULL, 'UL_giaiphapSanpham.jpg', '2020-03-16 09:57:54', '2020-03-17 04:15:14'),
(9, NULL, NULL, 'BH_banner1ThuanThanhDichVU.jpg', '2020-03-17 07:23:01', '2020-03-18 08:17:16'),
(10, NULL, NULL, 'Banner-3.jpg', '2020-03-20 09:37:24', '2020-03-20 09:37:24');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quyen` tinyint(4) NOT NULL DEFAULT '0',
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cmnd` int(11) DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `postoffice` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `quyen`, `avatar`, `cmnd`, `address`, `phone`, `postoffice`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(5, 'Nguyễn Viết Việt', 'nvviet59@gmail.com', 1, 'wI_black-and-white-iphone-6-smartphone-apple-9052.jpg', NULL, NULL, 12312, NULL, NULL, '$2y$10$I2JsoHuCtBI5VJi8w7AFWO6P0gK859f2FerNebS/iHhjX25bAQ7ou', NULL, '2020-03-06 23:44:28', '2020-03-11 01:49:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `abouts_about_slug_index` (`about_slug`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `slides`
--
ALTER TABLE `slides`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
