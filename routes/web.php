<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('admin/dangnhap','UserController@getDangnhapAdmin');
Route::post('admin/dangnhap','UserController@postDangnhapAdmin')->name('dangnhap');
Route::get('admin/logout','UserController@DangXuatAdmin');

// Route::get('/', function () {
//     return view('admin.layouts.index');
// });

Route::group(['prefix'=>'admin','middleware'=>'adminLogin'],function(){

	Route::group(['prefix'=>'category'],function(){
		Route::get('danhsach','AdminCategoryController@index')->name('category.list');

        Route::get('/them','AdminCategoryController@create')->name('category.create');
        Route::post('/them','AdminCategoryController@store')->name('category.insert');

        Route::get('/sua/{id}','AdminCategoryController@edit')->name('category.edit');
        Route::post('/sua/{id}','AdminCategoryController@update');

        Route::get('/{xoa}/{id}','AdminCategoryController@getXoa')->name('deleteCate');
	});

	Route::group(['prefix'=>'news'],function(){
		Route::get('danhsach','AdminNewController@index')->name('new.list');

		Route::get('/them','AdminNewController@create')->name('new.create');
		Route::post('/them','AdminNewController@store');

		Route::get('/sua/{id}','AdminNewController@edit')->name('new.edit');
		Route::post('/sua/{id}','AdminNewController@update');

		Route::get('/{xoa}/{id}','AdminNewController@getXoa')->name('deleteNew');
	});

	Route::group(['prefix'=>'slide'],function(){
		Route::get('danhsach','AdminSlideController@index')->name('slide.list');

		Route::get('/them','AdminSlideController@create')->name('slide.create');
		Route::post('/them','AdminSlideController@store');

		Route::get('sua/{id}','AdminSlideController@edit')->name('slide.edit');
		Route::post('sua/{id}','AdminSlideController@update');

		Route::get('/xoa/{id}','AdminSlideController@getXoa')->name('deleteSlide');
	});


	Route::group(['prefix'=>'user'],function(){
		Route::get('danhsach','UserController@index')->name('user.list');

		Route::get('/them','UserController@create')->name('user.create');
		Route::post('/them','UserController@store')->name('user.insert');

		Route::get('sua/{id}','UserController@edit')->name('user.edit');
		Route::post('sua/{id}','UserController@update');

		Route::get('/xoa/{id}','UserController@getXoa')->name('deleteUser');
	});


	//Liên Hệ
	Route::group(['prefix'=>'contact'],function(){

		Route::get('/','ContactController@index')->name('admin.get.list.contact');

    	Route::get('/{xoa}/{id}','ContactController@action')->name('deleteCt');
	});


	//About
	Route::group(['prefix'=>'about'],function(){

		Route::get('/','AboutController@index')->name('admin.get.list.about');
    	Route::get('/them','AboutController@create')->name('admin.get.create.about');
    	Route::post('/them','AboutController@store');

    	Route::get('/sua/{id}','AboutController@edit')->name('admin.get.edit.about');
    	Route::post('/sua/{id}','AboutController@update');

    	Route::get('/xoa/{id}','AboutController@getXoa')->name('deleteAb');
	});

    //Rating
    Route::group(['prefix'=>'rating'],function(){

        Route::get('/','RatingController@index')->name('admin.get.list.rating');
        Route::get('/them','RatingController@create')->name('admin.get.create.rating');
        Route::post('/them','RatingController@store');

        Route::get('/sua/{id}','RatingController@edit')->name('admin.get.edit.rating');
        Route::post('/sua/{id}','RatingController@update');

        Route::get('/xoa/{id}','RatingController@getXoa')->name('deleteRating');
    });

});

// Show dữ liệu ra trang chủ

Route::get('','HomeController@trangchu')->name('home');
// Route::get('/header','HomeController@header');
Route::get('/rightnew','HomeController@rightNew');

//Loại tin

Route::get('loai-tin/{id}/{slug}.html','HomeController@loaitin')->name('loaitin');
Route::get('tat-ca/{id}/{slug}.html','HomeController@tatca')->name('tatca');

//Blog
Route::get('/blog','HomeController@blog')->name('blog');


//Tìm kiếm
Route::get('timkiem','HomeController@timkiem')->name('list.new');

//Chi tiết tin
Route::get('tin-tuc/{id}/{slug}.html','HomeController@tintuc')->name('tintuc');

//Liên hệ
//Liên hệ
Route::get('lienhe','ContactController@getContact')->name('lienhe');
Route::post('lienhe','ContactController@saveContact');

